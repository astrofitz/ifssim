### What is this repository for? ###

This repository is for simulating lenslet-based integral field spectrometer data.  Use cases include the testing of data extraction algorithms.

### Whom do I talk to? ###

* Mike Fitzgerald <mpfitz@ucla.edu>