# code for simulation of GPI IFS spectra and pol
#
# Michael Fitzgerald (mpfitz@ucla.edu)


import os
import numpy as n
import matplotlib as mpl
import pylab
from astropy.io import fits

import logging
_log = logging.getLogger('gpi')

#import ipdb

import simulate
import psfmodel

data_dir = 'data/gpi/'



_GPIPolSimulatorBase = simulate.LensletIFSSimulator
class GPIPolSimulator(_GPIPolSimulatorBase):
    """
    A base class for simulating the GPI Polarimeter.  Uses a simple
    Gaussian as the PSF function.
    """
    def __init__(self):
        # call base class constructor
        _GPIPolSimulatorBase.__init__(self)

        # specify geometry
        #

        # spacing between lenslets on detector
        self.ld = 10. # [pix] displacement between pixels
        self.ldth = 24.8*n.pi/180. # [rad] angle relative to detector x axis (+ccw)

        # no dispersion in pol mode
        del self.dlam
        self.th_disp = 45.*n.pi/180. + self.ldth # [rad] (+ccw)
        self.polsep = 7.2 # [pix] separation between channels

        # detector size
        #self.ny, self.nx = 2048, 2048 # [pix], [pix]
        self.ny, self.nx = 256, 256 # [pix], [pix] TEMP
        self.ndetpix = self.nx*self.ny

        # number of lenslets across yp and xp axes
        self.nllyp = int(n.round((self.ny*n.cos(self.ldth)+
                                  self.nx*n.sin(self.ldth))/self.ld)) + 2
        self.nllxp = int(n.round((self.nx*n.cos(self.ldth)+
                                  self.ny*n.sin(self.ldth))/self.ld)) + 2
        self.nll = self.nllyp*self.nllxp

        # PSF parameters
        # FIXME  no longer used
        self.pwd = 2./2.3548 # [pix]  PSF width, x axis (dispersion)
        self.pwp = 2./2.3548 # [pix]  PSF width, y axis (perp. to dispersion)

        # shift on detector parameters
        self.shift_x = 0. # [pix]
        self.shift_y = 0. # [pix]

    def lenslet_lam_to_det(self, i, p):
        "lenslet index i and pol index p to detector y,x"
        y0, x0 = self.lenslet_to_det(i)
        ps = 2.*p-1.
        y = y0 + ps*self.polsep/2. * n.sin(self.th_disp)
        x = x0 + ps*self.polsep/2. * n.cos(self.th_disp)
        return y, x

    def get_supersampled_psf(self, i, pol_i):
        "get the super-sampled PSF at lenslet index i, pol cannel pol_i"
        # NOTE  pol_i unused here
        y0, x0 = self.lenslet_to_det(i)
        # NOTE  toy function for testing
        r = n.sqrt((y0-self.ny/2.)**2 + (x0-self.nx/2.)**2)
        sig = n.sqrt(1.5**2+(r/20.)**2)
        def toypsf(dx, dy):
            dr2 = dx**2+dy**2
            return 1./2./n.pi/sig**2*n.exp(-dr2/2./sig**2)

        # define # samples in fine grid for one pixel in detector coordinates
        s = 3.2
        yp, xp = n.mgrid[0:int(s*self.psf_box_size+2),0:int(s*self.psf_box_size+2)]
        # define center of PSF in prime coordinates
        ypc, xpc = yp.max()/2., xp.max()/2.

        # evaluate toy function on prime grid
        dy = (yp-ypc)/s
        dx = (xp-xpc)/s
        sspsf_samp = toypsf(dx, dy)

        # compute function for interpolating gridded super-sampled PSF
        from scipy.ndimage.interpolation import map_coordinates
        def mypsf(dx, dy):
            iy = dy*s+ypc
            ix = dx*s+xpc
            c = n.array((iy, ix))
            return map_coordinates(sspsf_samp, c)

        return mypsf

    def psf(self, dx, dy, lenslet_ind=None, lam=None):
        "interpolated PSF from supersampled PSF"
        # NOTE  use wavelength to encode pol channel
        sspsf = self.get_supersampled_psf(lenslet_ind, lam)
        return sspsf(dx, dy)

    def reformat_3d_to_2d(self, data):
        "reformat data from 2 spatial dimensions to 1"
        assert len(data.shape)==3
        w = self.lens2d-self.lens2d.min(axis=1)[:,n.newaxis]
        return data[w[0],w[1],:]

    def reformat_2d_to_3d(self, data):
        "reformat data from 1 spatial dimension to 2"
        assert len(data.shape)==2
        cube = n.zeros((self.nllyp,self.nllxp,2), dtype=n.float)+n.nan
        w = self.lens2d-self.lens2d.min(axis=1)[:,n.newaxis]
        cube[w[0],w[1],:] = data
        return cube

    def simulate_IFS_image(self, data, **kwargs):

        if len(data.shape)==3:
            # reformat cube to 1d spatially
            data = self.reformat_3d_to_2d(data)

        return _GPIPolSimulatorBase.simulate_IFS_image(self, n.array([0,1]), data, **kwargs)

    def simulate_WLS(self):
        raise NotImplementedError


_GPIPolWithCalSimulatorBase = GPIPolSimulator
class GPIPolWithCalSimulator(_GPIPolWithCalSimulatorBase):
    """
    A class for simulating the GPI Polarimeter using Gaussians derived
    from pipeline-processed polcal files for the spot PSFs.
    """
    def __init__(self, polcal_fn):
        # call base class constructor
        _GPIPolWithCalSimulatorBase.__init__(self)

        # specify geometry
        #

        # spacing between lenslets on detector
        del self.ld
        del self.ldth

        # no dispersion in pol mode
        del self.th_disp
        del self.polsep

        # load polcal info
        with fits.open(polcal_fn) as hl:
            spotpos = hl[1].data
            spotpos_pixels = hl[2].data
            spotpos_pixvals = hl[3].data

        # detector size
        self.ny, self.nx = 2048, 2048 # [pix], [pix]
        #self.ny, self.nx = 1024, 1024 # [pix], [pix] TEMP
        #self.ny, self.nx = 768, 768 # [pix], [pix] TEMP
        #self.ny, self.nx = 512, 512 # [pix], [pix] TEMP
        #self.ny, self.nx = 256, 256 # [pix], [pix] TEMP
        self.ndetpix = self.nx*self.ny

        # get lenslets where both pol channels fall on detector
        xc = spotpos[:,:,:,0]
        yc = spotpos[:,:,:,1]
        good = (xc>=0.) & (xc < self.nx) & (yc>=0.) & (yc < self.ny)
        good = n.all(good, axis=0)
        w = n.nonzero(good)

        # PSF parameters
        del self.pwd
        del self.pwp
        self.gauss_parm = spotpos[:,w[0],w[1],:]

        # get list of 2d lenslet indices for each 1d lenslet index
        self.lens2d = n.array(w)

        # number of lenslets across yp and xp axes
        self.nllyp = self.lens2d[0,:].max()-self.lens2d[0,:].min()+1
        self.nllxp = self.lens2d[1,:].max()-self.lens2d[1,:].min()+1
        self.nll = self.gauss_parm.shape[1]

    def lenslet_to_det(self, i):
        "lenslet index i to detector y,x"
        # FIXME  shift?
        x0 = self.gauss_parm[:,0].mean()
        y0 = self.gauss_parm[:,1].mean()
        return y0, x0

    def lenslet_lam_to_det(self, i, p):
        "lenslet index i and pol index p to detector y,x"
        # FIXME  shift?
        x0 = self.gauss_parm[p,i,0]
        y0 = self.gauss_parm[p,i,1]
        return y0, x0

    def get_supersampled_psf(self, i, pol_i):
        "get the super-sampled PSF at lenslet index i, pol cannel pol_i"
        raise NotImplementedError

    def psf(self, dx, dy, lenslet_ind=None, lam=None):
        assert lenslet_ind is not None
        assert lam is not None

        # get gaussian_parameters
        ang, sigx, sigy = self.gauss_parm[lam, lenslet_ind, 2:]
        ang *= -n.pi/180. # [deg] -> [rad]
        sigx *= 1./2.3548
        sigy *= 1./2.3548

        a = n.cos(ang)**2/2./sigx**2 + n.sin(ang)**2/2./sigy**2
        b = -n.sin(2.*ang)/4./sigx**2 + n.sin(2.*ang)/4./sigy**2
        c = n.sin(ang)**2/2./sigx**2 + n.cos(ang)**2/2./sigy**2

        vals = n.exp(-(a*dx**2-2.*b*dx*dy+c*dy**2))
        return vals


_GPIPolMlensSimulatorBase = GPIPolWithCalSimulator
class GPIPolMlensSimulator(_GPIPolMlensSimulatorBase):
    """
    A class for simulating the GPI Polarimeter using super-sampled
    microlens PSFs derived from the pipeline.  The PSFs are refined by
    fitting for offsets and coordinate transformations for each spot.
    """
    def __init__(self, polcal_fn, mlens_fn, offset_fn=None):
        # call base class constructor
        _GPIPolMlensSimulatorBase.__init__(self, polcal_fn)

        # load microlens PSF data
        psfdata = fits.getdata(mlens_fn)
        self.mlens_values = psfdata['VALUES'] # array of PSF values
        self.mlens_xcoords = psfdata['XCOORDS'] # [pix] x coordinates of super-sampled PSF, centered on PSF
        self.mlens_ycoords = psfdata['YCOORDS'] # [pix] y coordinates of super-sampled PSF, centered on PSF
        self.mlens_ID = psfdata['ID'] # lenslet / channel

        # filter out bad PSFs
        s = self.mlens_values.shape
        w = n.nonzero(self.mlens_values.reshape(s[0],s[1]*s[2]).sum(axis=1)>0.)[0]
        if len(w) < s[0]:
            _log.warning("removing {} bad lenslet PSFs (out of {})".format(s[0]-len(w), s[0]))
            self.mlens_values = self.mlens_values[w,:,:]
            self.mlens_xcoords = self.mlens_xcoords[w,:]
            self.mlens_ycoords = self.mlens_ycoords[w,:]
            self.mlens_ID = self.mlens_ID[w,:]

        # normalize mlens PSFs
        s = self.mlens_values.shape
        self.mlens_values /= self.mlens_values.reshape(s[0],s[1]*s[2]).sum(axis=1)[:,n.newaxis,n.newaxis]
        # normalize to detector grid (rather than supersampled grid)
        dely = self.mlens_ycoords[:,1]-self.mlens_ycoords[:,0]
        delx = self.mlens_xcoords[:,1]-self.mlens_xcoords[:,0]
        self.mlens_values /= (delx*dely)[:,n.newaxis,n.newaxis]

        ## # TEMP
        ## # smooth microlens PSFs
        ## from scipy.ndimage import gaussian_filter
        ## fwhm = 1.5 # [pix]
        ## sig = fwhm/2.3548 # [pix]
        ## for i in range(s[0]):
        ##     self.mlens_values[i,:,:] = gaussian_filter(self.mlens_values[i,:,:], sig)

        # load offsets
        if offset_fn is not None:
            with fits.open(offset_fn, mode='readonly') as hl:
                self.offsets = hl[0].data
                self.matrices = hl[1].data
            ## print(self.nll)
            ## print(self.offsets.shape[0])
            ## print(self.matrices.shape[0])
            assert self.offsets.shape[0] == self.nll
            assert self.matrices.shape[0] == self.nll
        else:
            self.offsets = None
            self.matrices = None

    def get_supersampled_psf(self, i, pol_i):
        "get the super-sampled PSF at lenslet index i, pol cannel pol_i"

        # get 2d lenslet coordinates
        c = self.lens2d[:,i]

        # find index of nearest supersampled PSF
        r2 = n.sum((self.mlens_ID[:,1:None:-1]-c[n.newaxis,:])**2,axis=1)
        j = n.argmin(r2)

        # define grid for this position
        yp, xp = n.meshgrid(self.mlens_ycoords[j,:], self.mlens_xcoords[j,:])
        dely = self.mlens_ycoords[j,1]-self.mlens_ycoords[j,0]
        delx = self.mlens_xcoords[j,1]-self.mlens_xcoords[j,0]
        miny = self.mlens_ycoords[j,0]
        minx = self.mlens_xcoords[j,0]

        # get supersampled psf on prime grid
        sspsf_samp = self.mlens_values[j,:,:]

        # compute function for interpolating gridded super-sampled PSF
        from scipy.ndimage.interpolation import map_coordinates
        def mypsf(dx, dy):
            iy = (dy-miny)/dely
            ix = (dx-minx)/delx
            c = n.array((iy, ix))
            return map_coordinates(sspsf_samp, c)

        return mypsf

    def psf(self, dx, dy, lenslet_ind=None, lam=None):
        ## if self.offsets is None and self.matrices is None:
        ##     _log.warn('Using uninitialized offsets.')
        if self.offsets is not None:
            ox, oy = self.offsets[lenslet_ind,lam,:]
        else:
            ox, oy = 0., 0.
        if self.matrices is not None:
            a, b, c, d = self.matrices[lenslet_ind,lam,:]
        else:
            a, b, c, d = 1., 0., 0., 1.
        u = a*dx + b*dy
        v = c*dx + d*dy
        absdetJ = n.abs(a*d-b*c) # determinant of Jacobian
        if absdetJ == 0.: # bad values
            _log.warning("bad values {0}, {1}".format(lenslet_ind, lam))
            u, v = dx, dy
            ox, oy = 0., 0.
            absdetJ = 1.
        return absdetJ*GPIPolSimulator.psf(self, u-ox, v-oy,
                                           lenslet_ind=lenslet_ind,
                                           lam=lam) # uses supersampled PSF

    def get_offsets(self, im, var, offset_fn=None):
        "routine for fitting for offsets between microlens PSFs and polcal Gaussians"

        def worker(input, output):
            for i in iter(input.get, 'STOP'):
                if (i % 100) == 0:
                    _log.info("getting offsets for lenslet {0}/{1}".format(i,self.nll))
                for j in (0,1):
                    # get position for this lenslet/channel
                    y, x = self.lenslet_lam_to_det(i, j)

                    # get coordinates for PSF patch
                    by = n.max((int(y)-self.psf_box_size//2,0))
                    ey = n.min((int(y)+self.psf_box_size//2,self.ny-1))
                    bx = n.max((int(x)-self.psf_box_size//2,0))
                    ex = n.min((int(x)+self.psf_box_size//2,self.nx-1))
                    wy, wx = n.mgrid[by:ey,bx:ex]
                    wy, wx = wy.flatten(), wx.flatten()
                    wg = (wy, wx)

                    # define model function
                    def model_im(p):
                        # unpack parameters
                        dx, dy, s, offs, a, b, c, d = p
                        # get PSF
                        ddx = wx-x
                        ddy = wy-y
                        u = a*ddx + b*ddy
                        v = c*ddx + d*ddy
                        psf = s*self.psf(u-dx, v-dy, lenslet_ind=i, lam=j)
                        return psf+offs

                    # extract patch of data
                    dim = im[wg]
                    stdim = n.sqrt(var[wg])

                    # define comparison function
                    def fit_fn(p):
                        resid = (dim-model_im(p))/stdim
                        w = n.nonzero(~n.isnan(resid))
                        return resid[w]

                    # perform fit
                    from scipy.optimize import leastsq
                    sp = n.array((-0.04,-0.07,n.nansum(dim),10,1.,1e-4,1e-4,1.))
                    p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                                          full_output=True,
                                                          )
                    if ier not in (1,2,3):
                        _log.debug('{}, {} bad fit! err: {}, {}'.format(i,j, ier, mesg))
                        _log.debug(p_opt)
                    if n.any(n.abs(p_opt[0:2]) > 1):
                        _log.debug('{0}, {1}  bad fit!'.format(i,j))
                        p_opt *= 0.

                    output.put((i, j, p_opt))

        def feeder(input):
            "place locations on input queue"
            for i in range(self.nll):
                input.put(i, True)
            _log.debug('feeder finished')

        import multiprocessing as mp
        n_process = mp.cpu_count()
        n_max = n_process*8
        _log.debug("{0} processes".format(n_process))
        inqueue = mp.Queue(n_max)
        outqueue = mp.Queue()

        # start worker processes
        for i in range(n_process):
            mp.Process(target=worker, args=(inqueue, outqueue)).start()

        # start feeder
        feedp = mp.Process(target=feeder, args=(inqueue,))
        feedp.start()

        # collect output
        opt_parms = n.empty((self.nll, 2, 8), dtype=n.float)
        for k in range(2*self.nll):
            i, j, p_opt = outqueue.get()
            opt_parms[i,j,:] = p_opt

        # kill worker processes
        _log.debug('received all data; killing workers ...')
        for i in range(n_process):
            inqueue.put('STOP')

        # block until feeder finished
        _log.debug('waiting for feeder to finish ...')
        feedp.join(1.)
        _log.debug('... done.')

        # output offsets
        self.offsets = opt_parms[:,:,0:2]
        self.matrices = opt_parms[:,:,4:8]
        other = opt_parms[:,:,2:4]
        if offset_fn is not None:
            hl = fits.HDUList([fits.PrimaryHDU(self.offsets),
                               fits.ImageHDU(self.matrices),
                               fits.ImageHDU(other),
                               ])
            hl.writeto(offset_fn, overwrite=True)


_GPIPolPatchModelMlensSimulatorBase = GPIPolWithCalSimulator
class GPIPolPatchModelMlensSimulator(_GPIPolPatchModelMlensSimulatorBase):
    """
    A class for simulating the GPI Polarimeter using a model for the
    microlens PSF.  The model is set up by first fitting to
    white-light data.
    """
    def __init__(self, polcal_fn, model_fn=None, zern_order=None):
        # call base class constructor
        _GPIPolModelMlensSimulatorBase.__init__(self, polcal_fn)
        
        if zern_order is None:
            # zernike order not given
            if model_fn is None:
                zern_order = 3
            else:
                # figure out from model_fn file
                with fits.open(model_fn, mode='readonly') as hl:
                    if hl[0].data is None: # GPI-pipeline-written
                        n_z = hl[3].data.shape[2]+3 # number of zernike coeff
                    else: # Python-pipeline-written
                        n_z = hl[2].data.shape[2]+3 # number of zernike coeff
                from zernike import get_order
                zern_order = get_order(n_z)

        self.psfmodel = psfmodel.GPIPSFModel(zern_order=zern_order)

        # load model parameters
        if model_fn is not None:
            with fits.open(model_fn, mode='readonly') as hl:
                if hl[0].data is None: # GPI-pipeline-written
                    self.offsets = hl[1].data
                    self.other = hl[2].data
                    self.zernikes = hl[3].data
                else: # Python-pipeline-written
                    self.offsets = hl[0].data
                    self.other = hl[1].data
                    self.zernikes = hl[2].data
            assert self.offsets.shape == (self.nll, 2, 2)
            assert self.other.shape == (self.nll, 2, 1)
            assert self.zernikes.shape == (self.nll, 2, self.psfmodel.n_z-3)
        else:
            self.offsets = n.zeros((self.nll, 2, 2), dtype=n.float)
            self.other = None
            self.zernikes = n.zeros((self.nll, 2, self.psfmodel.n_z-3), dtype=n.float)

    def get_supersampled_psf(self, i, pol_i):
        "get the super-sampled PSF at lenslet index i, pol cannel pol_i"

        # get zernike coefficients for this position
        zern_coeff = self.zernikes[i,pol_i,:]

        # get coordinates of PSF patch
        xx_psf = self.psfmodel.xx_psf_t
        yy_psf = self.psfmodel.yy_psf_t
        dely = yy_psf[1,0]-yy_psf[0,0]
        delx = xx_psf[0,1]-xx_psf[0,0]
        miny = yy_psf[0,0]
        minx = xx_psf[0,0]

        # get supersampled PSF on prime grid
        sspsf_samp = self.psfmodel.get_psf(zern_coeff)

        # compute function for interpolating gridded super-sampled PSF
        from scipy.ndimage.interpolation import map_coordinates
        def mypsf(dx, dy):
            iy = (dy-miny)/dely
            ix = (dx-minx)/delx
            c = n.array((iy, ix))
            return map_coordinates(sspsf_samp, c)

        return mypsf

    def psf(self, dx, dy, lenslet_ind=None, lam=None):
        ## if self.offsets is None and self.other is None:
        ##     _log.warn('Using uninitialized model.')
        ox, oy = self.offsets[lenslet_ind,lam,:]
        return GPIPolSimulator.psf(self, dx-ox, dy-oy,
                                   lenslet_ind=lenslet_ind,
                                   lam=lam) # uses supersampled PSF


    def get_model_mlens(self, im, var, model_fn=None):
        "routine for fitting for offsets, other parameters between microlens PSFs and polcal exposure"
        from scipy.optimize import leastsq
        from poly import get_n_coeff, forward_2d_poly

        # STEP 1: Fit each lenslet individually using default starting
        # parameters.  Fit occurs in 2 passes, first to
        # position/scale, then to position/scale/zernike.
        _log.info('fitting first pass lenslet models')

        def worker(input, output):
            for i in iter(input.get, 'STOP'):
                if (i % 100) == 0:
                    _log.info("getting offsets for lenslet {0}/{1}".format(i,self.nll))
                for j in (0,1):
                    # get position for this lenslet/channel
                    y, x = self.lenslet_lam_to_det(i, j)
                    
                    # get coordinates for PSF patch
                    by = n.max((int(y)-self.psf_box_size//2,0))
                    ey = n.min((int(y)+self.psf_box_size//2,self.ny-1))
                    bx = n.max((int(x)-self.psf_box_size//2,0))
                    ex = n.min((int(x)+self.psf_box_size//2,self.nx-1))
                    wy, wx = n.mgrid[by:ey,bx:ex]
                    wy, wx = wy.flatten(), wx.flatten()
                    wg = (wy, wx)

                    # extract patch of data
                    dim = im[wg]
                    stdim = n.sqrt(var[wg])

                    # first, fit only position, scale
                    #

                    # set zernike coefficients to zero
                    self.zernikes[i,j,:] = 0.

                    # define model function
                    def model_im(p):
                        # unpack parameters
                        dx, dy, s = p[0:3] # position, scale
                        # set shift offsets
                        self.offsets[i,j,0] = dx
                        self.offsets[i,j,1] = dy
                        # get PSF
                        psf = s*self.psf(wx-x, wy-y, lenslet_ind=i, lam=j)
                        return psf

                    # define comparison function
                    def fit_fn(p):
                        resid = (dim-model_im(p))/stdim
                        w = n.nonzero(~n.isnan(resid))
                        return resid[w]
                    
                    # perform fit
                    sp = n.array((-0.04,-0.07,n.nansum(dim)))
                    
                    p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                                          full_output=True,
                                                          )
                    
                    if ier not in (1,2,3):
                        _log.debug('{}, {} bad fit! err: {}, {}'.format(i,j, ier, mesg))
                        _log.debug(p_opt)
                    if n.any(n.abs(p_opt[0:2]) > 1): # >1 pix displacement
                        _log.debug('{0}, {1}  bad fit! pass 1'.format(i,j))
                        p_opt *= 0.
                    
                    # now fit all parameters
                    #

                    # define model function
                    def model_im(p):
                        # unpack parameters
                        dx, dy, s = p[0:3] # position, scale, background
                        zc = p[3:] # zernike coefficients
                        # set shift offsets
                        self.offsets[i,j,0] = dx
                        self.offsets[i,j,1] = dy
                        # set zernike coefficients
                        self.zernikes[i,j,:] = zc
                        # get PSF
                        psf = s*self.psf(wx-x, wy-y, lenslet_ind=i, lam=j)
                        return psf

                    # define comparison function
                    def fit_fn(p):
                        resid = (dim-model_im(p))/stdim
                        w = n.nonzero(~n.isnan(resid))
                        return resid[w]
                    
                    # perform fit
                    sp = n.concatenate((p_opt,
                                        0.01*n.ones(self.psfmodel.n_z-3)))
                    p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                                          full_output=True,
                                                          )
                    if ier not in (1,2,3):
                        _log.debug('{}, {} bad fit! err: {}, {}'.format(i,j, ier, mesg))
                        _log.debug(p_opt)
                    if n.any(n.abs(p_opt[0:2]) > 1): # >1 pix displacement
                        _log.debug('{0}, {1}  bad fit! pass 2'.format(i,j))

                    # store best-fit parameters
                    output.put((i, j, p_opt))
                    
        """
        def feeder(input):
            "place locations on input queue"
            for i in range(self.nll):
                input.put(i, True)
            _log.debug('feeder finished')
            """

        import multiprocessing as mp
        n_process = mp.cpu_count()
        n_max = n_process*8
        _log.debug("{0} processes".format(n_process))
        inqueue = mp.Queue(n_max)
        outqueue = mp.Queue()

        # start worker processes
        for i in range(n_process):
            mp.Process(target=worker, args=(inqueue, outqueue)).start()
            
        # Shelley's attempt at combining feeder and output checker
        opt_parms = n.empty((self.nll, 2, 3+self.psfmodel.n_z-3), dtype=n.float)
        
        for i in range(self.nll):
            inqueue.put(i, True)
            ip, j, p_opt = outqueue.get()
            opt_parms[ip,j,:] = p_opt
            ip, j, p_opt = outqueue.get()
            opt_parms[ip,j,:] = p_opt
        
        # kill worker processes
        _log.debug('received all data; killing workers ...')
        for i in range(n_process):
            inqueue.put('STOP')

        # block until feeder finished
        #_log.debug('waiting for feeder to finish ...')
        #feedp.join(1.)
        #_log.debug('... done.')

        # store
        offsets = opt_parms[:,:,0:2]
        other = opt_parms[:,:,2:3]
        zernikes = opt_parms[:,:,3:]

        # STEP 2: Fit polynomial model to zernike parameters for each
        # lenslet.
        _log.info('fitting polynomial zernike model...')

        # number of polynomial coefficients
        #poly_deg = 2
        poly_deg = 3
        n_poly = get_n_coeff(poly_deg)

        def model(p):
            zernike_parms = n.reshape(p, (self.psfmodel.n_z-3,2,n_poly))
            iy = self.lens2d[0,:]
            ix = self.lens2d[1,:]
            zz = n.empty((self.psfmodel.n_z-3,2,self.nll), dtype=n.float)
            for i in range(self.psfmodel.n_z-3):
                for j in range(2):
                    zz[i,j,:] = forward_2d_poly(ix, iy, zernike_parms[i,j,:])
            return zz

        d_zernikes = n.transpose(zernikes, (2,1,0))
        def fit_fn(p):
            zz = model(p) # get model values
            resid = zz-d_zernikes
            return resid.flatten()

        # starting parameters
        sp = n.ones((self.psfmodel.n_z-3)*2*n_poly, dtype=n.float)*1e-9

        # perform fit
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              full_output=True,
                                              )
        if ier not in (1,2,3):
            _log.debug('{} {}'.format(ier, mesg))

        # best-fit zernike parameters
        zernike_mod = model(p_opt)


        # STEP 3: Fit each lenslet individually using starting
        # parameters derived from previous best-fit parameters for
        # scale/locaiton and polynomial fit for zernikes.  Fit occurs
        # in 1 pass for each lenslet, in contrast to step 1.
        _log.info('fitting second pass lenslet models')

        # starting parameters
        start_parms = opt_parms.copy()
        start_parms[:,:,3:] = zernike_mod.transpose(2,1,0)

        def worker(input, output):
            for i, spdata in iter(input.get, 'STOP'):
                if (i % 100) == 0:
                    _log.info("getting offsets for lenslet {0}/{1}".format(i,self.nll))
                for j in (0,1):
                    # get position for this lenslet/channel
                    y, x = self.lenslet_lam_to_det(i, j)

                    # get coordinates for PSF patch
                    by = n.max((int(y)-self.psf_box_size//2,0))
                    ey = n.min((int(y)+self.psf_box_size//2,self.ny-1))
                    bx = n.max((int(x)-self.psf_box_size//2,0))
                    ex = n.min((int(x)+self.psf_box_size//2,self.nx-1))
                    wy, wx = n.mgrid[by:ey,bx:ex]
                    wy, wx = wy.flatten(), wx.flatten()
                    wg = (wy, wx)

                    # extract patch of data
                    dim = im[wg]
                    stdim = n.sqrt(var[wg])

                    # define model function
                    def model_im(p):
                        # unpack parameters
                        dx, dy, s = p[0:3] # position, scale, background
                        zc = p[3:] # zernike coefficients
                        # set shift offsets
                        self.offsets[i,j,0] = dx
                        self.offsets[i,j,1] = dy
                        # set zernike coefficients
                        self.zernikes[i,j,:] = zc
                        # get PSF
                        psf = s*self.psf(wx-x, wy-y, lenslet_ind=i, lam=j)
                        return psf

                    # define comparison function
                    def fit_fn(p):
                        resid = (dim-model_im(p))/stdim
                        w = n.nonzero(~n.isnan(resid))
                        return resid[w]

                    # perform fit
                    sp = spdata[j]
                    p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                                          full_output=True,
                                                          )
                    if ier not in (1,2,3):
                        _log.debug('{}, {} bad fit! err: {}, {}'.format(i,j, ier, mesg))
                        _log.debug(p_opt)
                    if n.any(n.abs(p_opt[0:2]) > 1): # >1 pix displacement
                        _log.debug('{0}, {1}  bad fit! pass 3'.format(i,j))

                    # store best-fit parameters
                    output.put((i, j, p_opt))

        def feeder(input):
            "place locations on input queue"
            for i in range(self.nll):
                spdata = (start_parms[i,0,:],
                          start_parms[i,1,:])
                input.put((i,spdata), True)
            _log.debug('feeder finished')

        import multiprocessing as mp
        n_process = mp.cpu_count()
        n_max = n_process*8
        _log.debug("{0} processes".format(n_process))
        inqueue = mp.Queue(n_max)
        outqueue = mp.Queue()

        # start worker processes
        for i in range(n_process):
            mp.Process(target=worker, args=(inqueue, outqueue)).start()

        # start feeder
        feedp = mp.Process(target=feeder, args=(inqueue,))
        feedp.start()

        # collect output
        opt_parms = n.empty((self.nll, 2, 3+self.psfmodel.n_z-3), dtype=n.float)
        for k in range(2*self.nll):
            i, j, p_opt = outqueue.get()
            opt_parms[i,j,:] = p_opt

        # kill worker processes
        _log.debug('received all data; killing workers ...')
        for i in range(n_process):
            inqueue.put('STOP')

        # block until feeder finished
        _log.debug('waiting for feeder to finish ...')
        feedp.join(1.)
        _log.debug('... done.')
        
        # output offsets
        self.offsets = opt_parms[:,:,0:2]
        self.other = opt_parms[:,:,2:3]
        self.zernikes = opt_parms[:,:,3:]
        
        if model_fn is not None:
            hl = fits.HDUList([fits.PrimaryHDU(self.offsets),
                               fits.ImageHDU(self.other),
                               fits.ImageHDU(self.zernikes),
                               ])
            hl.writeto(model_fn, overwrite=True)


_GPIPolModelMlensSimulatorBase = GPIPolWithCalSimulator
class GPIPolModelMlensSimulator(_GPIPolModelMlensSimulatorBase):
    """
    A class for simulating the GPI Polarimeter using a model for the
    microlens PSF.  The model is set up by first fitting to
    white-light data.
    """
    def __init__(self, polcal_fn, model_fn=None, zern_order=None):
        # call base class constructor
        _GPIPolModelMlensSimulatorBase.__init__(self, polcal_fn)
        if zern_order is None:
            # zernike order not given
            if model_fn is None:
                zern_order = 3
            else:
                # figure out from model_fn file
                with fits.open(model_fn, mode='readonly') as hl:
                    n_z = hl[1].data.shape[0]+3 # number of zernike coeff
                from zernike import get_order
                zern_order = get_order(n_z)

        self.psfmodel = psfmodel.GPIPSFModel(zern_order=zern_order)

        # number of polynomial coefficients
        from poly import get_n_coeff
        #self.poly_deg = 2
        self.poly_deg = 3
        self.n_poly = get_n_coeff(self.poly_deg)

        # load model parameters
        if model_fn is not None:
            with fits.open(model_fn, mode='readonly') as hl:
                self.location_parms = hl[0].data
                self.zernike_parms = hl[1].data
                self.other_parms = hl[2].data
            assert self.location_parms.shape == (2, 2, self.n_poly)
            assert self.other_parms.shape == (2, self.n_poly)
            assert self.zernike_parms.shape == (self.psfmodel.n_z-3, 2, self.n_poly)
        else:
            # FIXME  are there more sane defaults?
            self.location_parms = None
            self.zernike_parms = None
            self.other = None

    def lenslet_to_det(self, i):
        "lenslet index i to detector y,x"
        # average position of two pol spots
        y0, x0 = self.lenslet_lam_to_det(i, 0)
        y1, x1 = self.lenslet_lam_to_det(i, 1)
        return (y0+y1)/2., (x0+x1)/2.

    def lenslet_lam_to_det(self, i, p):
        "lenslet index i and pol index p to detector y,x"
        # convert 1d lenslet index to 2d indices
        iy, ix = self.lens2d[:,i]
        # use polynomial model on indices to get x, y locations
        from poly import forward_2d_poly_vec
        if n.isscalar(p) or (len(p)==1):
            a, b = self.location_parms[0,p,:], self.location_parms[1,p,:]
            x0, y0 = forward_2d_poly_vec(ix, iy, a, b)
        else:
            x0, y0 = [], []
            for pp in p:
                a, b = self.location_parms[0,pp,:], self.location_parms[1,pp,:]
                xx, yy = forward_2d_poly_vec(ix, iy, a, b)
                x0.append(xx)
                y0.append(yy)
            x0 = n.array(x0)
            y0 = n.array(y0)
        return y0, x0

    def get_supersampled_psf(self, i, pol_i):
        "get the super-sampled PSF at lenslet index i, pol cannel pol_i"

        # convert 1d lenslet index to 2d indices
        iy, ix = self.lens2d[:,i]
        # get zernike coefficients for this position
        from poly import forward_2d_poly
        zern_coeff = n.array([forward_2d_poly(ix, iy, self.zernike_parms[j,pol_i,:]) \
                              for j in range(self.zernike_parms.shape[0])])

        # get coordinates of PSF thumbnail patch
        xx_psf = self.psfmodel.xx_psf_t
        yy_psf = self.psfmodel.yy_psf_t
        dely = yy_psf[1,0]-yy_psf[0,0]
        delx = xx_psf[0,1]-xx_psf[0,0]
        miny = yy_psf[0,0]
        minx = xx_psf[0,0]

        # get supersampled PSF on thumbnail grid
        sspsf_samp = self.psfmodel.get_psf(zern_coeff)

        # compute function for interpolating gridded super-sampled PSF
        from scipy.ndimage.interpolation import map_coordinates
        def mypsf(dx, dy):
            iy = (dy-miny)/dely
            ix = (dx-minx)/delx
            c = n.array((iy, ix))
            return map_coordinates(sspsf_samp, c)

        return mypsf

    def psf(self, dx, dy, lenslet_ind=None, lam=None):
        return GPIPolSimulator.psf(self, dx, dy,
                                   lenslet_ind=lenslet_ind,
                                   lam=lam) # uses supersampled PSF


    def get_model_mlens(self, im, var, model_fn=None, patch_model_fn=None):
        "routine for fitting for offsets, other parameters between microlens PSFs and polcal exposure"
        from scipy.optimize import leastsq
        from poly import forward_2d_poly, forward_2d_poly_vec, get_ca

        assert patch_model_fn is not None
        with fits.open(patch_model_fn) as hl:
            pm_offsets = hl[0].data
            pm_other = hl[1].data
            pm_zernikes = hl[2].data

        stdim = n.sqrt(var)

        # fit polynomial to polcal gaussian positions
        #
        _log.info('fitting initial position model...')

        def model(p):
            location_parms = n.reshape(p, (2,2,self.n_poly))
            iy = self.lens2d[0,:]
            ix = self.lens2d[1,:]
            a0 = location_parms[0,0,:]
            b0 = location_parms[1,0,:]
            a1 = location_parms[0,1,:]
            b1 = location_parms[1,1,:]
            x0, y0 = forward_2d_poly_vec(ix, iy, a0, b0)
            x1, y1 = forward_2d_poly_vec(ix, iy, a1, b1)
            return x0, y0, x1, y1

        def fit_fn(p):
            # get model values
            x0, y0, x1, y1 = model(p)
            # get data values
            x0d = self.gauss_parm[0,:,0]
            y0d = self.gauss_parm[0,:,1]
            x1d = self.gauss_parm[1,:,0]
            y1d = self.gauss_parm[1,:,1]
            resid = n.concatenate((x0-x0d,
                                   y0-y0d,
                                   x1-x1d,
                                   y1-y1d))
            return resid

        # starting parameters
        sp = n.zeros((2*2*self.n_poly), dtype=n.float)+1e-3

        # perform fit
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              full_output=True,
                                              )
        if ier not in (1,2,3):
            _log.debug('{} {]'.format(ier, mesg))

        # store best-fit location parameters
        location_parms = n.reshape(p_opt, (2,2,self.n_poly))


        # fit polynomial to patch model scale parameter
        #
        _log.info('fitting initial scale model...')

        def model(p):
            scale_parms = n.reshape(p, (2,self.n_poly))
            iy = self.lens2d[0,:]
            ix = self.lens2d[1,:]
            a0 = scale_parms[0,:]
            a1 = scale_parms[1,:]
            s0 = forward_2d_poly(ix, iy, a0)
            s1 = forward_2d_poly(ix, iy, a1)
            return s0, s1

        def fit_fn(p):
            # get model values
            s0, s1 = model(p)
            # get data values
            d0 = pm_other[:,0,0]
            d1 = pm_other[:,1,0]
            resid = n.concatenate((s0-d0,
                                   s1-d1))
            return resid

        # starting parameters
        sp = n.zeros((2, self.n_poly), dtype=n.float) + 1e-5
        sp[:,0] = 2e4
        sp = sp.reshape((2*self.n_poly,))

        # perform fit
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              full_output=True,
                                              )
        if ier not in (1,2,3):
            _log.debug('{} {}'.format(ier, mesg))

        # store best-fit location parameters
        scale_parms = n.reshape(p_opt, (2,self.n_poly))


        # fit polynomial to patch model zernikes
        #
        _log.info('fitting initial zernike model...')

        def model(p):
            zernike_parms = n.reshape(p, (self.psfmodel.n_z-3,2,self.n_poly))
            iy = self.lens2d[0,:]
            ix = self.lens2d[1,:]
            zz = n.empty((self.psfmodel.n_z-3,2,self.nll), dtype=n.float)
            for i in range(self.psfmodel.n_z-3):
                for j in range(2):
                    zz[i,j,:] = forward_2d_poly(ix, iy, zernike_parms[i,j,:])
            return zz

        d_zernikes = n.transpose(pm_zernikes, (2,1,0))
        def fit_fn(p):
            zz = model(p) # get model values
            resid = zz-d_zernikes
            return resid.flatten()

        # starting parameters
        sp = n.ones((self.psfmodel.n_z-3)*2*self.n_poly, dtype=n.float)*1e-9

        # perform fit
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              full_output=True,
                                              )
        if ier not in (1,2,3):
            _log.debug('{} {}'.format(ier, mesg))

        # store best-fit zernike parameters
        zernike_parms = n.reshape(p_opt, (self.psfmodel.n_z-3,2,self.n_poly))


        ## # TEMP----
        ## # store model parameters
        ## self.location_parms = location_parms
        ## self.zernike_parms = zernike_parms
        ## if model_fn is not None:
        ##     hl = fits.HDUList([fits.PrimaryHDU(location_parms),
        ##                        fits.ImageHDU(zernike_parms),
        ##                        fits.ImageHDU(scale_parms),
        ##                        ])
        ##     hl.writeto(model_fn, overwrite=True)

        ## return 
        ## # ----TEMP



        # code for fitting all parameters to data
        #

        shapes = [location_parms.shape,
                  zernike_parms.shape,
                  scale_parms.shape,
                  ]
        n_parms = [n.product(s) for s in shapes]
        cn_parms = n.cumsum(n_parms)

        def pack_params(*args):
            return n.concatenate([arg.flatten() for arg in args])

        def unpack_params(p):
            location_parms = p[0:cn_parms[0]].reshape(shapes[0])
            zernike_parms = p[cn_parms[0]:cn_parms[1]].reshape(shapes[1])
            scale_parms = p[cn_parms[1]:cn_parms[2]].reshape(shapes[2])
            return location_parms, zernike_parms, scale_parms

        def model_im(p):
            # unpack parameters
            location_parms, zernike_parms, scale_parms = unpack_params(p)
            self.location_parms = location_parms
            self.zernike_parms = zernike_parms
            # compute scale for each spot
            iy, ix = self.lens2d[0,:], self.lens2d[1,:] # convert 1d lenslet index to 2d indices
            scales0 = forward_2d_poly(ix, iy, scale_parms[0,:])
            scales1 = forward_2d_poly(ix, iy, scale_parms[1,:])
            data = n.array((scales0, scales1)).T
            # simulate image
            im = self.simulate_IFS_image(data, multiprocess=True)
            return im

        def fit_fn(p):
            mim = model_im(p)
            resid = (im-mim)/stdim
            w = n.nonzero(~n.isnan(resid))
            return resid[w]

        # construct starting parameter vector
        sp = pack_params(location_parms, zernike_parms, scale_parms)
        _log.info("{0} total parameters".format(len(sp)))

        # run least-squares fit
        p_opt, cov, info, mesg, ier = leastsq(fit_fn, sp.copy(),
                                              full_output=True,
                                              )
        if ier not in (1,2,3):
            _log.debug('{} {}'.format(ier, mesg))
        location_parms, zernike_parms, scale_parms = unpack_params(p_opt)

        # store model parameters
        self.location_parms = location_parms
        self.zernike_parms = zernike_parms
        if model_fn is not None:
            hl = fits.HDUList([fits.PrimaryHDU(location_parms),
                               fits.ImageHDU(zernike_parms),
                               fits.ImageHDU(scale_parms),
                               ])
            hl.writeto(model_fn, overwrite=True)




_GPIPolExtractorBase = simulate.Extractor
class GPIPolExtractor(_GPIPolExtractorBase):
    def __init__(self, sim, badpix_fn=None, badpix=None):
        if badpix is None:
            # load GPI bad pixel map
            #badpix = (fits.getdata(badpix_fn) != 0)
            badpix = (fits.getdata(badpix_fn) > 2)
        elif badpix_fn is not None:
            _log.error('both badpix_fn and badpix are specified')
            raise Exception
        if not (badpix.shape == (sim.ny, sim.nx)):
            _log.warning("badpix array not same shape, taking subregion")
            badpix = badpix[0:sim.ny,0:sim.nx]

        # set up extractor
        _GPIPolExtractorBase.__init__(self, sim, n.array([0,1]), use_wls=False, badpix=badpix)

    def extract(self, im, **kwargs):
        # get extracted data (nlens by 2)
        data = _GPIPolExtractorBase.extract(self, im, **kwargs)
        cube = self.sim.reformat_2d_to_3d(data)
        return cube



def gpi_pipeline_generate_spot_model(pc_fn,
                                     im, badpix_in, im_std,
                                     zern_order=4,
                                     ):
    "hook for gpi pipeline to generate a spot model"
    
    ## # TEMP
    with open('temp-genspotmodel.dat','wb') as f:
        import pickle
        pickle.dump(pc_fn, f)
        pickle.dump(im, f)
        pickle.dump(badpix_in, f)
        pickle.dump(im_std, f)
        pickle.dump(zern_order, f)

    badpix = (badpix_in > 2)
    im_var = im_std**2
    _log.debug(pc_fn)#TEMP
    _log.debug(im)#TEMP
    _log.debug(badpix_in)#TEMP
    _log.debug(im_std)#TEMP
    _log.debug(zern_order)#TEMP
    _log.debug(im_var)#TEMP

    # mark bad pixels
    w = n.nonzero(badpix)
    im[w] = n.nan

    # set up simulator
    sim = GPIPolPatchModelMlensSimulator(pc_fn, zern_order=zern_order)

    # compute model
    sim.get_model_mlens(im, im_var, model_fn=None)

    return sim.offsets.flatten(), sim.other.flatten(), sim.zernikes.flatten()


def gpi_pipeline_extract(im, im_var, badpix_in, offsets, other, zernikes, pc_fn, subtract_stray=False, temp_store=True):
    "hook for gpi pipeline to extract a polarization datacube"
    if temp_store:
        import pickle
        with open('temp.dat', 'wb') as f:
            pickle.dump(im, f)
            pickle.dump(im_var, f)
            pickle.dump(badpix_in, f)
            pickle.dump(offsets, f)
            pickle.dump(other, f)
            pickle.dump(zernikes, f)
            pickle.dump(pc_fn, f)

    # figure out zernike order
    from zernike import get_order
    n_z = zernikes.shape[2]+3
    zern_order = get_order(n_z)

    # set up simulator
    sim = GPIPolPatchModelMlensSimulator(pc_fn, zern_order=zern_order)
    sim.offsets = offsets
    sim.other = other
    sim.zernikes = zernikes

    # subtract stray light
    if subtract_stray:
        # get places where no light should fall
        cube = n.ones((sim.nllyp,sim.nllxp,2), dtype=n.float)
        uim = sim.simulate_IFS_image(cube, multiprocess=True)
        thresh = 1e-3
        w = n.nonzero(uim < thresh)
        bim = n.zeros((sim.ny,sim.nx), dtype=n.float)+n.nan
        bim[w] = im[w]
        import numpy.ma as ma
        bim = ma.masked_where(n.isnan(bim), bim)
        # fill in lenslet locations
        from image import median_replace
        i = 0
        _log.info('filling in missing data, pass {0}'.format(i))
        rbim = median_replace(bim)
        while n.any(rbim.mask):
            i += 1
            _log.info('filling in missing data, pass {0}'.format(i))
            rbim = median_replace(rbim)
        stray = rbim.filled()
        # subtract from data
        im -= stray

    # set up extractor
    badpix = (badpix_in > 2)
    extractor = GPIPolExtractor(sim, badpix=badpix)

    # run extraction
    cube = extractor.extract(im, variance=im_var)
    cube = n.transpose(cube, (2,0,1))

    # compute data quality cube
    dataq = n.zeros_like(cube) # FIXME

    return cube, dataq

def test_pipeline_extract():
    import pickle
    with open('temp.dat', 'rb') as f:
        im = pickle.load(f)
        im_var = pickle.load(f)
        badpix_in = pickle.load(f)
        offsets = pickle.load(f)
        other = pickle.load(f)
        zernikes = pickle.load(f)
        pc_fn = pickle.load(f)

    import ipdb


    cube, dataq = gpi_pipeline_extract(im, im_var, badpix_in, offsets, other, zernikes, pc_fn, temp_store=False)
    ipdb.set_trace()


def test_simulate_pol_image(simulator, fignum=0, **kwargs):
    "a simple test script for running a simulator"
    # construct data for different channels
    data = n.zeros((simulator.nll, 2), dtype=n.float)
    data[:,0] = 2.
    data[:,1] = 1.

    # simulate
    im = simulator.simulate_IFS_image(data, **kwargs)

    # show results
    fig = pylab.figure(fignum)
    fig.clear()
    ax = fig.add_subplot(111)
    ax.imshow(im,
              interpolation='nearest',
              )
    pylab.draw()
    pylab.show()

    return im



# When changing files/exposures, edit lines:
#   data_fn_root = data_dir+'S20161010S00##'
#   fn = 'temp#.fits'
# When using function to generate extracted datacube for the Gaussian model, edit lines:
#   uncomment #sim = GPIPolWithCalSimulator(pc_fn)
#   comment sim = GPIPolMlensSimulator(pc_fn, psf_fn, offset_fn=offset_fn)
#   comment ax.scatter(sim.mlens_ID[:,1], sim.mlens_ID[:,0], c='r')
#   uncomment #fits.writeto(data_fn_root+'-gcube.fits', n.transpose(cube, (2,0,1)), overwrite=True) 
#   comment fits.writeto(data_fn_root+'-mcube.fits', n.transpose(cube, (2,0,1)), overwrite=True)
#   edit line fn = 'temp#.fits' to become fn = 'tempg#.fits'
#   edit line fits.writeto(data_fn_root+'-simcal.fits', sim_im, overwrite=True) to become fits.writeto(data_fn_root+'-gsimcal.fits', sim_im, overwrite=True)
#   edit line fits.writeto(data_fn_root+'-imcal.fits', im, overwrite=True) to become fits.writeto(data_fn_root+'-gimcal.fits', im, overwrite=True)

def test_microlens_PSF(simcls,
                       pc_fn, psf_fn, data_fn_root,
                       suffix='',
                       use_offsets=False,
                       subtract_stray=False,
                       zern_order=None,
                       redo_all=False,
                       ):
    "fit the microlens simulator using data, then extract and re-simulate"

    out_fn_root = data_fn_root+suffix

    # load raw image data
    raw_fn = data_fn_root+'-input.fits.gz'
    im = fits.getdata(raw_fn)

    # bad pixel map
    bp_fn = data_fn_root+'-badpix.fits.gz'
    badpix_in = fits.getdata(bp_fn)

    # uncertainty map
    unc_fn = data_fn_root+'-im_uncert.fits.gz'
    im_uncert = fits.getdata(unc_fn)

     # TEMP
    with open('temp-genspotmodel.dat','rb') as f:
        _log.warning('overriding input with temp-genspotmodel.dat!')
        import pickle
        pc_fn = pickle.load(f)
        im = pickle.load(f)
        badpix_in = pickle.load(f)
        im_uncert = pickle.load(f)
        zern_order = pickle.load(f)


    # get into correct format
    #badpix = (badpix_in != 0)
    badpix = (badpix_in > 2)
    im_var = im_uncert**2

    # mark bad pixels
    w = n.nonzero(badpix)
    im[w] = n.nan


    # extract windows if simulator is undersized
    #
    # get dummy instance
    if issubclass(simcls, GPIPolMlensSimulator):
        sim = simcls(pc_fn, psf_fn)
    elif issubclass(simcls, GPIPolWithCalSimulator):
        sim = simcls(pc_fn)
    else:
        sim = simcls()
    # get window size
    window = n.index_exp[0:sim.ny,0:sim.nx]
    # extract windows
    im = im[window] # get window
    badpix = badpix[window] # get window
    im_var = im_var[window] # get window


    # set up simulator
    if issubclass(simcls, GPIPolMlensSimulator):
        if use_offsets:
            offset_fn = out_fn_root+'-offsets.fits'
            if redo_all or not os.access(offset_fn, os.R_OK):
                sim = simcls(pc_fn, psf_fn)
                sim.get_offsets(im, im_var, offset_fn=offset_fn)
        else:
            model_fn = None
        sim = simcls(pc_fn, psf_fn, offset_fn=offset_fn)
    elif issubclass(simcls, GPIPolPatchModelMlensSimulator):
        if use_offsets:
            model_fn = out_fn_root+'-patchmodel.fits'
            if redo_all or not os.access(model_fn, os.R_OK):
                sim = simcls(pc_fn, zern_order=zern_order)
                sim.get_model_mlens(im, im_var, model_fn=model_fn)
        else:
            model_fn = None
        sim = simcls(pc_fn, model_fn=model_fn, zern_order=zern_order)
    elif issubclass(simcls, GPIPolModelMlensSimulator):
        if use_offsets:
            patch_model_fn = out_fn_root.replace('PMM','PPMM')+'-patchmodel.fits'
            assert os.access(patch_model_fn, os.R_OK)
            model_fn = out_fn_root+'-model.fits'
            if redo_all or not os.access(model_fn, os.R_OK):
                sim = simcls(pc_fn, zern_order=zern_order)
                sim.get_model_mlens(im, im_var,
                                    model_fn=model_fn,
                                    patch_model_fn=patch_model_fn,
                                    )
        else:
            offset_fn = None
        sim = simcls(pc_fn, model_fn=model_fn, zern_order=zern_order)
    elif issubclass(simcls, GPIPolWithCalSimulator):
        sim = simcls(pc_fn)
    else:
        sim = simcls()

    ## # TEMP
    ## test_simulate_pol_image(sim, multiprocess=True)
    ## assert False


    # subtract out stray light map
    if subtract_stray:
        fn = out_fn_root+'-stray.fits'
        if redo_all or not os.access(fn, os.R_OK):
            # get places where no light should fall
            cube = n.ones((sim.nllyp,sim.nllxp,2), dtype=n.float)
            uim = sim.simulate_IFS_image(cube, multiprocess=True)
            thresh = 1e-3
            w = n.nonzero(uim < thresh)
            bim = n.zeros((sim.ny,sim.nx), dtype=n.float)+n.nan
            bim[w] = im[w]
            import numpy.ma as ma
            bim = ma.masked_where(n.isnan(bim), bim)

            # fill in lenslet locations
            from image import median_replace
            i = 0
            _log.info('filling in missing data, pass {0}'.format(i))
            rbim = median_replace(bim)
            while n.any(rbim.mask):
                i += 1
                _log.info('filling in missing data, pass {0}'.format(i))
                rbim = median_replace(rbim)
            stray = rbim.filled()
            # write out
            fits.writeto(fn, stray, overwrite=True)
        else:
            # load
            stray = fits.getdata(fn)

        # subtract from data
        im -= stray


    ## # generate a simulated test image
    ## im = test_simulate_pol_image(sim, fignum=2)
    ## fits.writeto('test.fits', im, overwrite=True)
    ## #fits.writeto('testmlens.fits', sim.mlens_values)

    # set up extractor
    extractor = GPIPolExtractor(sim, badpix_fn=bp_fn)


    # extract data cube
    cube = extractor.extract(im, variance=im_var)
    fits.writeto(out_fn_root+'-cube.fits', n.transpose(cube, (2,0,1)),
                 overwrite=True)

    # simulate detector image
    sim_im = sim.simulate_IFS_image(cube, multiprocess=True)

    # write output
    fits.writeto(out_fn_root+'-simcal.fits', sim_im, overwrite=True)
    fits.writeto(out_fn_root+'-imcal.fits', im, overwrite=True)

    # examine results
    examine_test_results(out_fn_root)


def examine_test_results(data_fn_root, show_boundaries=False):
    "look at differences between data and model in detector frame"

    # load test results
    cube = fits.getdata(data_fn_root+'-cube.fits')
    sim_im = fits.getdata(data_fn_root+'-simcal.fits')
    im = fits.getdata(data_fn_root+'-imcal.fits')

    if show_boundaries:
        idmap_fn = data_dir+'ssIDmap.fits'
        if not os.access(idmap_fn, os.R_OK):
            ssIDmap = examine_regions()
            fits.writeto(idmap_fn, ssIDmap)
        else:
            ssIDmap = fits.getdata(idmap_fn)

    v = 10. # number of sigma for difference display
    vmin, vmax = 0., 1e4 # for data and model
    w = 25 # [pix] window half-size
    diff_cmap = mpl.cm.jet
    im_cmap = mpl.cm.gist_stern

    # difference image
    diff_im = im - sim_im
    # local rms?
    ## import numpy.ma as ma
    ## mim = ma.masked_where(n.isnan(diff_im), diff_im)
    from scipy.ndimage import percentile_filter, median_filter
    boxsize = 20
    sig_im = median_filter(n.abs(diff_im), size=boxsize, cval=0.)
    ## pf1 = percentile_filter(diff_im, 50-34.1, size=boxsize, cval=0.)
    ## pf2 = percentile_filter(diff_im, 50+34.1, size=boxsize, cval=0.)
    ## sig_im = (pf2-pf1)/2. # half distance between locations of 1sig confidence

    # show difference in detector
    #diff_std = diff_im.std()
    diff_std = n.nanmedian(n.abs(diff_im))
    fig1 = pylab.figure(1, figsize=(8,4))
    fig1.clear()
    fig2 = pylab.figure(2, figsize=(12,4))
    fig2.clear()
    ax1 = fig1.add_subplot(121)
    ax2 = fig1.add_subplot(122)
    ax3 = fig2.add_subplot(131)
    ax4 = fig2.add_subplot(132)
    ax5 = fig2.add_subplot(133)
    for ax in (ax1, ax2, ax5):
        ax.imshow(diff_im,
                  interpolation='nearest',
                  vmin=-v*diff_std,
                  vmax=v*diff_std,
                  cmap=diff_cmap,
                  )
    ax3.imshow(im,
               interpolation='nearest',
               vmin=vmin, vmax=vmax,
               cmap=im_cmap,
               )
    ax4.imshow(sim_im,
               interpolation='nearest',
               vmin=vmin, vmax=vmax,
               cmap=im_cmap,
               )
    for ax in (ax2, ax3, ax4, ax5):
        ax.axis((im.shape[1]/2-w, im.shape[1]/2+w, im.shape[0]/2-w, im.shape[0]/2+w))
    ax1.set_title('resisudals')
    ax2.set_title('resisudals zoom')
    ax3.set_title('data')
    ax4.set_title('model')
    ax5.set_title('residuals')
    pylab.draw()
    pylab.show()

    if diff_im.shape==(2048,2048):
        fig = pylab.figure(3, figsize=(6,6))
        fig.clear()
        ax = fig.add_subplot(111)
        from skimage.segmentation import mark_boundaries
        from skimage.exposure import rescale_intensity
        d = 5.
        vmin, vmax = n.nanpercentile(diff_im, (d,100.-d))
        scim = rescale_intensity(diff_im, in_range=(vmin,vmax), out_range=(0.,1.))
        cmap = mpl.cm.jet
        cim = cmap(scim)[:,:,0:3]
        if show_boundaries:
            im = mark_boundaries(cim,
                                 ssIDmap.astype(n.int),
                                 color=mpl.colors.colorConverter.to_rgb('m'),
                                 )
        else:
            im = cim
        ax.imshow(im,
                  interpolation='nearest',
                  )
        if show_boundaries:
            ax.set_title('residuals with mlens boundaries')
        else:
            ax.set_title('residuals')
        pylab.draw()
        pylab.show()


        fig = pylab.figure(4, figsize=(5,6))
        fig.clear()
        ax = fig.add_subplot(111)
        from skimage.segmentation import mark_boundaries
        from skimage.exposure import rescale_intensity
        d = .2
        vmin, vmax = n.nanpercentile(sig_im, (d,100.-d))
        cmap = mpl.cm.magma
        if show_boundaries:
            scim = rescale_intensity(sig_im, in_range=(vmin,vmax), out_range=(0.,1.))
            cim = cmap(scim)[:,:,0:3]
            im = mark_boundaries(cim,
                                 ssIDmap.astype(n.int),
                                 color=mpl.colors.colorConverter.to_rgb('b'),
                                 )
            ax.imshow(im,
                      interpolation='nearest',
                      )
        else:
            imobj = ax.imshow(sig_im,
                              interpolation='nearest',
                              cmap=cmap,
                              vmin=vmin, vmax=vmax,
                              )
            # show colorbar
            cbar = fig.colorbar(imobj,
                                orientation='horizontal',
                                )
        # annotate
        if show_boundaries:
            ax.set_title('filtered residuals with mlens boundaries')
        else:
            ax.set_title('filtered residuals')
        pylab.draw()
        pylab.show()

    #ipdb.set_trace()


def examine_regions():
    pc_fn = data_dir+'S20161010S0012_H-polcal.fits.gz'
    psf_fn = data_dir+'150615_pol_highres_1.65_psf_structure.fits.gz'
    sim = GPIPolMlensSimulator(pc_fn, psf_fn)

    y, x = n.mgrid[0:sim.ny,0:sim.nx]

    # get IDs of nearest lenslets
    _log.info('mapping pixels to lenslets...')
    lensletIDmap = n.empty((sim.ny,sim.nx), dtype=n.int)
    for j in range(sim.ny):
        _log.debug('row {0}'.format(j))
        r2 = n.empty((sim.nll,sim.nx), dtype=n.float)
        for i in range(sim.nll):
            dy1, dx1 = sim.lenslet_lam_to_det(i,0)
            dy2, dx2 = sim.lenslet_lam_to_det(i,1)
            r21 = (dx1-x[j,:])**2 + (dy1-y[j,:])**2
            r22 = (dx2-x[j,:])**2 + (dy2-y[j,:])**2
            r2[i,:] = n.array((r21,r22)).min(axis=0)
        lensletIDmap[j,:] = n.argmin(r2, axis=0)

    # translate to IDs of nearest supersampled PSFs
    _log.info('mapping lenslets to supersampled PSFs...')
    ssIDmap = n.empty((sim.ny,sim.nx), dtype=n.float)
    for i in range(sim.nll):
        # get 2d lenslet coordinates
        c = sim.lens2d[:,i]
        # find index of nearest supersampled PSF
        r2 = n.sum((sim.mlens_ID[:,1:None:-1]-c[n.newaxis,:])**2,axis=1)
        j = n.argmin(r2)

        # fill in ID map
        w = n.nonzero(lensletIDmap == i)
        ssIDmap[w] = j

    return ssIDmap




def examine_offsets(pc_fn, psf_fn, data_fn_root):
    "look at the results of offset/transformation fitting"

    offset_fn = data_fn_root+'-offsets.fits'

    with fits.open(offset_fn, mode='readonly') as hl:
        offsets = hl[0].data
        matrices = hl[1].data
        other = hl[2].data

    sim = GPIPolMlensSimulator(pc_fn, psf_fn, offset_fn=offset_fn)

    dx = sim.reformat_2d_to_3d(offsets[:,:,0])
    dy = sim.reformat_2d_to_3d(offsets[:,:,1])
    s = sim.reformat_2d_to_3d(other[:,:,0])
    offs = sim.reformat_2d_to_3d(other[:,:,1])

    a = sim.reformat_2d_to_3d(matrices[:,:,0])
    b = sim.reformat_2d_to_3d(matrices[:,:,1])
    c = sim.reformat_2d_to_3d(matrices[:,:,2])
    d = sim.reformat_2d_to_3d(matrices[:,:,3])

    fig = pylab.figure(0, figsize=(8,8))
    fig.clear()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(223, sharex=ax1, sharey=ax1)
    ax4 = fig.add_subplot(224, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3, ax4), (dx, dy, s, offs)):
        ax.imshow(x[:,:,0],
                  interpolation='nearest',
                  cmap=mpl.cm.viridis,
                  )
    ax1.set_title('dx')
    ax2.set_title('dy')
    ax3.set_title('s')
    ax4.set_title('offs')
    pylab.draw()
    pylab.show()


    fig = pylab.figure(1, figsize=(8,8))
    fig.clear()
    ax1 = fig.add_subplot(221)
    ax2 = fig.add_subplot(222, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(223, sharex=ax1, sharey=ax1)
    ax4 = fig.add_subplot(224, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3, ax4), (a, b, c, d)):
        ax.imshow(x[:,:,0],
                  interpolation='nearest',
                  cmap=mpl.cm.viridis,
                  )
    ax1.set_title('a')
    ax2.set_title('b')
    ax3.set_title('c')
    ax4.set_title('d')
    pylab.draw()
    pylab.show()




def examine_patchmodel(pc_fn, data_fn_root):
    "look at the results of microlens PSF model fitting"

    model_fn = data_fn_root+'-patchmodel.fits'

    with fits.open(model_fn, mode='readonly') as hl:
        offsets = hl[0].data
        other = hl[1].data
        zernikes = hl[2].data

    sim = GPIPolPatchModelMlensSimulator(pc_fn, model_fn=model_fn)

    dx = sim.reformat_2d_to_3d(offsets[:,:,0])
    dy = sim.reformat_2d_to_3d(offsets[:,:,1])
    s = sim.reformat_2d_to_3d(other[:,:,0])

    z1 = sim.reformat_2d_to_3d(zernikes[:,:,0])
    z2 = sim.reformat_2d_to_3d(zernikes[:,:,1])
    z3 = sim.reformat_2d_to_3d(zernikes[:,:,2])

    fig = pylab.figure(0, figsize=(9,3))
    fig.clear()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(133, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3), (dx, dy, s)):
        ax.imshow(x[:,:,0],
                  interpolation='nearest',
                  cmap=mpl.cm.viridis,
                  )
    ax1.set_title('dx')
    ax2.set_title('dy')
    ax3.set_title('s')
    pylab.draw()
    pylab.show()


    fig = pylab.figure(1, figsize=(9,3))
    fig.clear()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(133, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3), (z1, z2, z3)):
        ax.imshow(x[:,:,0],
        #ax.imshow(n.abs(x[:,:,0]), # TEMP
                  interpolation='nearest',
                  cmap=mpl.cm.viridis,
                  )
    ax1.set_title('z1')
    ax2.set_title('z2')
    ax3.set_title('z3')
    pylab.draw()
    pylab.show()


def examine_model(pc_fn, data_fn_root):
    "look at the results of microlens PSF model fitting"

    model_fn = data_fn_root+'-model.fits'

    with fits.open(model_fn, mode='readonly') as hl:
        location_parms = hl[0].data
        zernike_parms = hl[1].data
        scale_parms = hl[2].data

    sim = GPIPolModelMlensSimulator(pc_fn, model_fn=model_fn)

    iy = sim.lens2d[0,:]
    ix = sim.lens2d[1,:]

    from poly import forward_2d_poly
    a0 = scale_parms[0,:]
    a1 = scale_parms[1,:]
    s0 = forward_2d_poly(ix, iy, a0)
    s1 = forward_2d_poly(ix, iy, a1)
    s = sim.reformat_2d_to_3d(n.array((s0,s1)).T)

    zz = n.empty((sim.psfmodel.n_z-3,2,sim.nll), dtype=n.float)
    for i in range(sim.psfmodel.n_z-3):
        for j in range(2):
            zz[i,j,:] = forward_2d_poly(ix, iy, zernike_parms[i,j,:])
    z1 = sim.reformat_2d_to_3d(zz[0,:,:].T)
    z2 = sim.reformat_2d_to_3d(zz[1,:,:].T)
    z3 = sim.reformat_2d_to_3d(zz[2,:,:].T)

    fig = pylab.figure(0, figsize=(6,3))
    fig.clear()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2), (s[:,:,0], s[:,:,1])):
        ax.imshow(x,
                  interpolation='nearest',
                  cmap=mpl.cm.viridis,
                  )
    ax1.set_title('s0')
    ax2.set_title('s1')
    pylab.draw()
    pylab.show()


    fig = pylab.figure(1, figsize=(9,3))
    fig.clear()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(133, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3), (z1, z2, z3)):
        ax.imshow(x[:,:,0],
        #ax.imshow(n.abs(x[:,:,0]) # TEMP,
                  interpolation='nearest',
                  cmap=mpl.cm.viridis,
                  )
    ax1.set_title('z1')
    ax2.set_title('z2')
    ax3.set_title('z3')
    pylab.draw()
    pylab.show()


def compare_patch_and_poly(pc_fn, data_fn_root):
    "compare patch-based model and polynomial-based-model"

    patch_fn_root = data_fn_root+'-PPMM'
    poly_fn_root = data_fn_root+'-PMM'

    # prepare patch model
    #

    # load model and data
    patch_model_fn = patch_fn_root+'-patchmodel.fits'
    with fits.open(patch_model_fn, mode='readonly') as hl:
        patch_offsets = hl[0].data
        patch_other = hl[1].data
        patch_zernikes = hl[2].data
    patch_sim = GPIPolPatchModelMlensSimulator(pc_fn, model_fn=patch_model_fn)

    # compute location parameter distribution
    patch_dx = patch_sim.reformat_2d_to_3d(patch_offsets[:,:,0])
    patch_dy = patch_sim.reformat_2d_to_3d(patch_offsets[:,:,1])
    patch_x0 = patch_sim.reformat_2d_to_3d(patch_sim.gauss_parm[:,:,0].T)
    patch_y0 = patch_sim.reformat_2d_to_3d(patch_sim.gauss_parm[:,:,1].T)
    patch_x = patch_x0+patch_dx # FIXME  check sign
    patch_y = patch_y0+patch_dy # FIXME  check sign

    # compute scale parameter distribution
    patch_s = patch_sim.reformat_2d_to_3d(patch_other[:,:,0])

    # compute zernike parameter distributions
    patch_z1 = patch_sim.reformat_2d_to_3d(patch_zernikes[:,:,0])
    patch_z2 = patch_sim.reformat_2d_to_3d(patch_zernikes[:,:,1])
    patch_z3 = patch_sim.reformat_2d_to_3d(patch_zernikes[:,:,2])

    # prepare poly model
    #

    # load model and data
    poly_model_fn = poly_fn_root+'-model.fits'
    with fits.open(poly_model_fn, mode='readonly') as hl:
        poly_location_parms = hl[0].data
        poly_zernike_parms = hl[1].data
        poly_scale_parms = hl[2].data
    poly_sim = GPIPolModelMlensSimulator(pc_fn, model_fn=poly_model_fn)

    iy = poly_sim.lens2d[0,:]
    ix = poly_sim.lens2d[1,:]

    # compute location parameter distribution
    loc = n.array([poly_sim.lenslet_lam_to_det(i, (0,1)) for i in range(poly_sim.nll)])
    poly_x = poly_sim.reformat_2d_to_3d(loc[:,1,:])
    poly_y = poly_sim.reformat_2d_to_3d(loc[:,0,:])

    # compute scale parameter distribution
    from poly import forward_2d_poly
    a0 = poly_scale_parms[0,:]
    a1 = poly_scale_parms[1,:]
    s0 = forward_2d_poly(ix, iy, a0)
    s1 = forward_2d_poly(ix, iy, a1)
    poly_s = poly_sim.reformat_2d_to_3d(n.array((s0,s1)).T)

    # compute zernike parameter distributions
    zz = n.empty((poly_sim.psfmodel.n_z-3,2,poly_sim.nll), dtype=n.float)
    for i in range(poly_sim.psfmodel.n_z-3):
        for j in range(2):
            zz[i,j,:] = forward_2d_poly(ix, iy, poly_zernike_parms[i,j,:])
    poly_z1 = poly_sim.reformat_2d_to_3d(zz[0,:,:].T)
    poly_z2 = poly_sim.reformat_2d_to_3d(zz[1,:,:].T)
    poly_z3 = poly_sim.reformat_2d_to_3d(zz[2,:,:].T)


    # compare parameter distributions
    #
    #cmap = mpl.cm.viridis
    cmap = mpl.cm.jet

    fig = pylab.figure(0, figsize=(9,3))
    fig.clear()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(133, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3), (patch_x-poly_x,
                                       patch_y-poly_y,
                                       patch_s-poly_s)):
        ax.imshow(x[:,:,0],
                  interpolation='nearest',
                  cmap=cmap,
                  )
    ax1.set_title('x')
    ax2.set_title('y')
    ax3.set_title('s')
    pylab.draw()
    pylab.show()


    fig = pylab.figure(1, figsize=(9,3))
    fig.clear()
    ax1 = fig.add_subplot(131)
    ax2 = fig.add_subplot(132, sharex=ax1, sharey=ax1)
    ax3 = fig.add_subplot(133, sharex=ax1, sharey=ax1)
    for ax, x in zip((ax1, ax2, ax3), (patch_z1-poly_z1,
                                       patch_z2-poly_z2,
                                       patch_z3-poly_z3)):
        ax.imshow(x[:,:,0],
                  interpolation='nearest',
                  cmap=cmap,
                  )
    ax1.set_title('z1')
    ax2.set_title('z2')
    ax3.set_title('z3')
    pylab.draw()
    pylab.show()

    #ipdb.set_trace()



# Calculating RMS
def find_RMS(array, remove_mean=False):
    if remove_mean is False:
        rms = n.sqrt(n.nanmean(n.square(array)))
    elif remove_mean is True:
        rms = n.sqrt(n.nanmean(n.square(array - n.nanmean(array))))
    return rms

# Finding fractional RMS on 9 patches
def patch_RMS(data_fn):
    patch_RMS = []
    patches = [
        (53,63,88,98), (57,67,140,150),
        (80,90,180,190), (95,105,60,70),
        (110,120,120,130), (135,145,195,205),
        (135,145,40,50), (185,195,100,110),
        (200,210,150,160)]
    data_cube = fits.getdata(data_fn)

    # Slicing data for a 10 x 10 pixel patch
    count_patch = 1

    for patch in patches:
        y_coord_1 = patch[0]
        y_coord_2 = patch[1]
        x_coord_1 = patch[2]
        x_coord_2 = patch[3]
    
        obj1_1 = (slice(None,1), slice(y_coord_1,y_coord_2), slice(x_coord_1,x_coord_2))
        obj1_2 = (slice(1,None), slice(y_coord_1,y_coord_2), slice(x_coord_1,x_coord_2))
    
        data_1 = data_cube[obj1_1]

        data_2 = data_cube[obj1_2]
    
        # rms on extracted model cubes and pipeline
        rms_1 = find_RMS(data_1, remove_mean=True)

        rms_2 = find_RMS(data_2, remove_mean=True)

        # fractional rms
        frac_rms_1 = rms_1 / n.mean(data_1)
        frac_rms_2 = rms_2 / n.mean(data_2)
        
        patch_RMS = n.append(patch_RMS, (frac_rms_1, frac_rms_2))

        count_patch += 1
    return(patch_RMS)
    print(patch_RMS)


def run_test_suite():
    suffixes = {GPIPolSimulator:'-P',
                GPIPolWithCalSimulator:'-PWC',
                GPIPolMlensSimulator:'-PM',
                GPIPolPatchModelMlensSimulator:'-PPMM',
                GPIPolModelMlensSimulator:'-PMM',
                }

    simcls_to_choose = [GPIPolWithCalSimulator, GPIPolMlensSimulator, GPIPolPatchModelMlensSimulator, GPIPolModelMlensSimulator]
    #simcls_to_choose = [GPIPolPatchModelMlensSimulator]
    suffixes_2 = ['-PPMM-cube', '-PM-cube', '-PWC-cube']
    suffixes_3 = ['PPMM', 'PM', 'PWC']
    
    for simcls in simcls_to_choose:
        pc_fn = data_dir+'S20161010S0012_H-polcal.fits.gz'
        psf_fn = data_dir+'150615_pol_highres_1.65_psf_structure.fits.gz'
        suffix = suffixes[simcls]
        #use_offsets = False
        use_offsets = True
        #subtract_stray = False
        subtract_stray = True
        #redo_all = False
        redo_all = True
        """ #loop for generating cube.fits files
        for i in range(8, 13):
            if len(str(i)) == 1:
                i = '0' + str(i)
            data_fn_root = data_dir+'S20161010S00'+str(i)
            test_microlens_PSF(simcls,
                       pc_fn, psf_fn, data_fn_root,
                       suffix=suffix,
                       use_offsets=use_offsets,
                       subtract_stray=subtract_stray,
                       redo_all=redo_all,
                       )"""
                       
    all_rms = []
    for i in range(8, 13): #loop for collecting all the rms values for all combinations of patch, file, exposure, and channel
        if len(str(i)) == 1:
            i = '0' + str(i)
        data_fn_root = data_dir+'S20161010S00'+str(i)
        for suffix in suffixes_3:
            data_fn = data_fn_root+'-'+suffix+'-cube.fits'
            file_RMS = patch_RMS(data_fn)
            all_rms = n.append(all_rms, file_RMS)
    all_rms = all_rms.reshape(3, 5 ,18)
    
    average_rms = []
    for j in range(0,3): #loop for calculating the average rms across all patches of each exposure and model combination (one for each channel)
        rms_to_use = []
        r_average = []
        for g in range(0, 5):
            for i in range(0, 2):
                rms_to_use = n.append(rms_to_use, all_rms[j,g,i::2])
        rms_to_use = rms_to_use.reshape(5,2,9)
        for h in range(0, 5):
            for f in range(0, 2):
                average = n.average(rms_to_use[h,f])
                r_average = n.append(r_average, average)
        r_average = r_average.reshape(5,2)
        average_rms = n.append(average_rms, r_average)
    average_rms = average_rms.reshape(3, 5, 2)
    
    stored_RMS = []  
    for j in range(0,3): #loop for collecting the average rms across all patches of each exposure and model combination in a record array
        for i in range(8, 13):
            w = i-8
            if len(str(i)) == 1:
                i = '0' + str(i)
            data_fn_root = data_dir+'S20161010S00'+str(i)
            data_fn = data_fn_root+'-'+suffix[j]+'-cube.fits'
            file_name = 'S20161010S00'+str(i)
            file_type = suffixes_3[j]
            stored_RMS = n.append(stored_RMS, (file_name, file_type, average_rms[j,w,0], average_rms[j,w,1]))
    number_of_rows = int(n.shape(stored_RMS)[0]/4)
    stored_RMS = stored_RMS.reshape((number_of_rows, 4))
    stored_RMS = [tuple(row) for row in stored_RMS]
    recarray_RMS = n.array(stored_RMS, dtype=[('file', "<U32"), ('model', "<U32"), ('rms_1', float), ('rms_2', float)])
    
    PPMM_rms_1 = recarray_RMS[:5]['rms_1']
    PPMM_rms_2 = recarray_RMS[:5]['rms_2']
    PM_rms_1 = recarray_RMS[5:10]['rms_1']
    PM_rms_2 = recarray_RMS[5:10]['rms_2']
    PWC_rms_1 = recarray_RMS[10:]['rms_1']
    PWC_rms_2 = recarray_RMS[10:]['rms_2']
    model_rms = [PPMM_rms_1, PPMM_rms_2, PM_rms_1, PM_rms_2, PWC_rms_1, PWC_rms_2]
    model_rms_names = ['PPMM_rms_1', 'PPMM_rms_2', 'PM_rms_1', 'PM_rms_2', 'PWC_rms_1', 'PWC_rms_2']
    model_means = []
    model_stds = []
    for model in range(0,6): #loop for generating a record array of the rms mean and standard deviation of each model (by channel)
        mean = n.mean(model_rms[model])
        std = n.std(model_rms[model])
        if 'PPMM_rms_1' in model_rms_names[model]:
            model_means = n.append(model_means, ['PPMM', mean])
            model_stds = n.append(model_stds, ['PPMM', std])
        elif 'PM_rms_1' in model_rms_names[model]:
            model_means = n.append(model_means, ['PM', mean])
            model_stds = n.append(model_stds, ['PM', std])
        elif 'PWC_rms_1' in model_rms_names[model]:
            model_means = n.append(model_means, ['PWC', mean])
            model_stds = n.append(model_stds, ['PWC', std])
        else:
            model_means = n.append(model_means, mean)
            model_stds = n.append(model_stds, std)
    model_means = n.array([tuple(row) for row in model_means.reshape(3,3)], dtype=[('model', "<U32"), ('channel 1', float), ('channel 2', float)])
    model_stds = n.array([tuple(row) for row in model_stds.reshape(3,3)], dtype=[('model', "<U32"), ('channel 1', float), ('channel 2', float)])
    
    from astropy.table import Table #create astropy tables for means and standard deviations
    mean_model = model_means['model']
    mean_channel_1 = model_means['channel 1']
    mean_channel_2 = model_means['channel 2']
    means_table = Table([mean_model, mean_channel_1, mean_channel_2], names = ('model','channel 1 mean','channel 2 mean'), meta={'name': 'Means table'})
    
    std_model = model_stds['model']
    std_channel_1 = model_stds['channel 1']
    std_channel_2 = model_stds['channel 2']
    stds_table = Table([std_model, std_channel_1, std_channel_2], names = ('model','channel 1 standard deviation','channel 2 standard deviation'), meta={'name': 'Standard deviation table'})
    
    print(means_table)
    print(stds_table)


if __name__=='__main__':
    logging.basicConfig(level=logging.DEBUG,
    #logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(name)-12s: %(levelname)-8s %(message)s',
                        )

    ## sim = GPIPolSimulator()
    ## im = test_simulate_pol_image(sim)

    #test_polcal()
    
    suffixes = {GPIPolSimulator:'-P',
                GPIPolWithCalSimulator:'-PWC',
                GPIPolMlensSimulator:'-PM',
                GPIPolPatchModelMlensSimulator:'-PPMM',
                GPIPolModelMlensSimulator:'-PMM',
                }
    #simcls = GPIPolWithCalSimulator
    #simcls = GPIPolMlensSimulator
    simcls = GPIPolPatchModelMlensSimulator
    #simcls = GPIPolModelMlensSimulator
    pc_fn = data_dir+'S20161010S0012_H-polcal.fits.gz'
    #pc_fn = os.path.expanduser('~/work/data/gpi/Reduced/calibrations/GPIDATA-Calibrations/2016B/S20161010S0012_H-polcal.fits')
    psf_fn = data_dir+'150615_pol_highres_1.65_psf_structure.fits.gz'
    #data_fn_root = data_dir+'S20161010S0012'
    data_fn_root = data_dir+'S20161010S0008'
    suffix = suffixes[simcls]
    #use_offsets = False
    use_offsets = True
    #subtract_stray = False
    subtract_stray = True
    
    #redo_all = False
    redo_all = True
    #zern_order = 3
    zern_order = 4

    ## sim = GPIPolModelMlensSimulator(pc_fn)
    ## im = test_simulate_pol_image(sim)

    test_microlens_PSF(simcls,
                       pc_fn, psf_fn, data_fn_root,
                       suffix=suffix,
                       use_offsets=use_offsets,
                       subtract_stray=subtract_stray,
                       redo_all=redo_all,
                       zern_order=zern_order,
                       )
    #examine_test_results(data_fn_root+suffix)
    #examine_regions()
    #examine_offsets(pc_fn, psf_fn, data_fn_root+suffix)
    #examine_patchmodel(pc_fn, data_fn_root+suffix)
    #examine_model(pc_fn, data_fn_root+suffix)
    #compare_patch_and_poly(pc_fn, data_fn_root)
    #patch_RMS()
    #run_test_suite()
    #test_pipeline_extract()
