# code for simulation of OSIRIS IFS spectra
#
# Michael Fitzgerald (mpfitz@ucla.edu)

import os
import numpy as n
import matplotlib as mpl
import pylab
from astropy.io import fits

import logging
_log = logging.getLogger('osiris')

import ipdb

import simulate
import psfmodel

data_dir = 'data/osiris/'
drp_dir = os.path.expanduser('~/work/OsirisDRP/')
gdrv_dir = os.path.expanduser('~/Google Drive/OSIRIS Pipeline Working Group/')

_OSIRISSimulatorBase = simulate.LensletIFSSimulator
class OSIRISSimulator(_OSIRISSimulatorBase):
    """
    A base class for simulating the OSIRIS IFS.  Uses a simple
    Gaussian as the PSF function.
    """
    def __init__(self):
        # call base class constructor
        _OSIRISSimulatorBase.__init__(self)

        # specify geometry
        #

        # detector data
        self.ny, self.nx = 2048, 2048 # [pix]
        self.ndetpix = self.nx*self.ny

        # rectification matrix data
        rectmat_fn = drp_dir+'tests/calib/s160416_c006___infl_Kbb_035.fits.gz'
        with fits.open(rectmat_fn) as hl:
            rm0 = hl[0].data
            rm1 = hl[1].data
            rm2 = hl[2].data
        self.nll = rm0.shape[0] # number of lenslets

        # load lenslet mapping
        lenslet_fn = drp_dir+'/testing_scripts/trace_spectra/kbb_2016_slice_to_lenslet.txt'
        lenslet_map = n.loadtxt(lenslet_fn, dtype=n.int)
        w = n.argsort(lenslet_map[:,0])
        self.lens2d = lenslet_map[w,1:3].T
        self.nllyp = self.lens2d[0,:].max()-self.lens2d[0,:].min()+1
        self.nllxp = self.lens2d[1,:].max()-self.lens2d[1,:].min()+1


        # load wavelength solution
        wave_fn = drp_dir+'/data/osiris_wave_coeffs_121110-151231.fits'
        self.wave_coeffs = fits.getdata(wave_fn)

        # define wavelength channels
        #
        disp = self.wave_coeffs[:,:,1].flatten() # [pix/nm]
        avg_disp = disp[n.nonzero(disp)].mean() # [pix/nm]
        self.nlam = 1700 # number of wavelength channels
        self.lams = n.linspace(1.965, 2.381, self.nlam, endpoint=True)# [um] wavelengths
        disp = 1e-3/(self.lams[1]-self.lams[0]) # [pix/nm]

        # load y position trace data
        #
        _log.info('loading trace data...')
        trace_fn = gdrv_dir+'Test Data/Trace/s150905_c003___infl_Kbb_035_trace.npy'
        tracedata = n.load(trace_fn, encoding='bytes').item(0)
        self.peaklocs = []
        self.isgood = []
        xx = n.arange(2048)
        from scipy.interpolate import interp1d
        for i in range(len(tracedata)):
            # get y position in each extracted set of rows
            slicestr = bytes('slice{0}'.format(i), 'utf-8')
            tfit, sampleLoc, peakLoc, lineProfile, fitParams, spectrum = tracedata[slicestr]
            # get interpolating function
            #w = n.nonzero(peakLoc)
            #w = n.arange(len(peakLoc))
            w = n.nonzero(n.logical_and((n.modf(peakLoc)[0] !=0), (spectrum > 0.1*spectrum.max())))
            x = xx[w]
            y = peakLoc[w]+rm0[i,0] # add row offset
            if len(w[0])==0:
                self.peaklocs.append(None)
                continue
            peakloc_fn = interp1d(x, y, bounds_error=False, fill_value=n.nan)
            self.peaklocs.append(peakloc_fn)
        _log.info('done loading trace data.')


        # PSF parameters
        self.psf_sig = 1./2.3548 # [pix]  FIXME

        #ipdb.set_trace() # TEMP

    def get_wave_x(self, i, lam):
        "compute x position given 1d lenslet index and wavelength [um]"
        # figure out 2d index given 1d index
        iy = self.lens2d[0,i]
        ix = self.lens2d[1,i]
        # get wavelength coefficients
        coeffs = self.wave_coeffs[iy,ix]
        # compute polynomial
        from numpy.polynomial.polynomial import polyval
        lam_nm = lam*1e3
        midwave = 2200. # defined in assembcube_000
        x = polyval(lam_nm-midwave, coeffs)
        return x

    def lenslet_to_det(self, i):
        "lenslet index i to detector y,x"
        raise NotImplementedError

    def lenslet_lam_to_det(self, i, lam):
        "lenslet index i and wavelength lam to detector y,x"
        # get x position
        x = self.get_wave_x(i, lam)
        # get y position at this x position
        peak_fn = self.peaklocs[i]
        if peak_fn is None:
            y = n.zeros_like(x)+n.nan
        else:
            y = peak_fn(x)
        return y, x

    def psf(self, dx, dy, lenslet_ind=None, lam=None):
        # get gaussian parameters
        sig = self.psf_sig
        # compute gaussian
        vals = n.exp(-(dx**2+dy**2)/2./sig**2)
        return vals

    def reformat_3d_to_2d(self, data):
        "reformat data from 2 spatial dimensions to 1"
        assert len(data.shape)==3
        w = self.lens2d-self.lens2d.min(axis=1)[:,n.newaxis]
        return data[w[0],w[1],:]

    def reformat_2d_to_3d(self, data):
        "reformat data from 1 spatial dimension to 2"
        assert len(data.shape)==2
        cube = n.zeros((self.nllyp,self.nllxp,2), dtype=n.float)+n.nan
        w = self.lens2d-self.lens2d.min(axis=1)[:,n.newaxis]
        cube[w[0],w[1],:] = data
        return cube

    def simulate_IFS_image(self, data, **kwargs):

        if len(data.shape)==3:
            # reformat cube to 1d spatially
            data = self.reformat_3d_to_2d(data)

        return _OSIRISSimulatorBase.simulate_IFS_image(self, self.lams, data, **kwargs)

    def simulate_WLS(self):
        raise NotImplementedError




_OSIRISExtractorBase = simulate.Extractor
class OSIRISExtractor(_OSIRISExtractorBase):
    def __init__(self, sim, badpix_fn=None, badpix=None):
        if badpix is None:
            # load bad pixel map
            badpix = (fits.getdata(badpix_fn) != 0)
            #badpix = (fits.getdata(badpix_fn) > 2)
        elif badpix_fn is not None:
            _log.error('both badpix_fn and badpix are specified')
            raise Exception
        if not (badpix.shape == (sim.ny, sim.nx)):
            _log.warning("badpix array not same shape, taking subregion")
            badpix = badpix[0:sim.ny,0:sim.nx]

        # set up extractor
        _OSIRISExtractorBase.__init__(self, sim, n.array([0,1]), use_wls=False, badpix=badpix)

    def extract(self, im, **kwargs):
        # get extracted data (nlens by 2)
        data = _OSIRISExtractorBase.extract(self, im, **kwargs)
        cube = self.sim.reformat_2d_to_3d(data)
        return cube



def test_simulate_image():
    "a simple test script for running a simulator"

    # get simulator instance
    sim = OSIRISSimulator()

    # construct data for different channels
    data = n.ones((sim.nll, sim.nlam), dtype=n.float)

    # simulate
    im = sim.simulate_IFS_image(data, multiprocess=True)

    # show results
    fig = pylab.figure(0)
    fig.clear()
    ax = fig.add_subplot(111)
    ax.imshow(im,
              interpolation='nearest',
              vmax=5.,
              )
    pylab.draw()
    pylab.show()

    return im


def test_peakloc():
    "examine peak locations from Tuan's fitter"

    rectmat_fn = drp_dir+'/tests/calib/s160416_c006___infl_Kbb_035.fits.gz'
    with fits.open(rectmat_fn) as hl:
        rm0 = hl[0].data
        rm1 = hl[1].data

    trace_fn = gdrv_dir+'/Test Data/Trace/s150905_c003___infl_Kbb_035_trace.npy'
    tracedata = n.load(trace_fn, encoding='bytes').item(0)
    peaklocs = []
    xx = n.arange(2048)
    for i in range(len(tracedata)):
        # get y position in each extracted set of rows
        slicestr = bytes('slice{0}'.format(i), 'utf-8')
        tfit, sampleLoc, peakLoc, lineProfile, fitParams, spectrum = tracedata[slicestr]
        #if n.any(peakLoc>20): ipdb.set_trace()
        #print(spectrum.max())
        #if i==500: ipdb.set_trace()#TEMP
        # get interpolating function
        #ipdb.set_trace()
        w = n.nonzero(n.logical_and((n.modf(peakLoc)[0] !=0), (spectrum > 0.1*spectrum.max())))
        #w = n.arange(len(peakLoc))
        x = xx[w]
        if n.any((peakLoc[w] < 0.) | (peakLoc[w] > (rm0[i,1]-rm0[i,0]))):
            print('bad fit on trace {0}'.format(i))
        y = peakLoc[w]+rm0[i,0] # add row offset
        peaklocs.append((y, x))

    fig = pylab.figure(0)
    fig.clear()
    ax = fig.add_subplot(111)
    for i, (y, x) in enumerate(peaklocs):
        if rm1[i] == 0: continue
        ax.scatter(x, y,
                   c='k',
                   alpha=0.8,
                   lw=0.5,
                   s=1.,
                   marker='.',
                   edgecolors='none',
                   )
    ax.set_aspect('equal')
    ax.axis((0,2048,0,2048))
    pylab.draw()
    pylab.show()

if __name__=='__main__':
    logging.basicConfig(level=logging.DEBUG,
    #logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(name)-12s: %(levelname)-8s %(message)s',
                        )

    #im = test_simulate_image()
    test_peakloc()
