# code for modeling microlens PSFs
# Michael Fitzgerald  (mpfitz@ucla.edu)

import numpy as n
import matplotlib as mpl
import pylab

from numpy.fft import fft2, ifft2, fftshift, rfft2, irfft2
import zernike

class PSFModel(object):

    D_tel = None # [um] telescope diameter   FIXME  should be projected Lyot
    D_tel_obs = None # [um] central obscuration diameter  FIXME  should be projected Lyot
    F_tel = None # focal ratio coming into lenslet array
    D_lenslet = None # [um] size of lenslet (roughly pitch)
    f_lenslet = None # [um] effective focal length of lenslet
    mag_backend = None # magnification of spectrometer backend to detector
    d_det_pix = None # [um] physical size of detector pixel
    ang_lens = None # [rad]  angle of lenslet array relative to detector (CW)

    def __init__(self, zern_order=3):
        # input quantities
        self.lam = 1.6 # [um] wavelength

        # derived quantities
        f_tel = self.F_tel*self.D_tel # [um] effective focal length of telescope to lenslet array
        th_diff = self.lam/self.D_tel # [rad]  diffraction-limited angular resolution on sky
        F_lenslet = self.f_lenslet/self.D_lenslet # focal ratio of lenslet
        th_lenslet = self.D_lenslet / f_tel # [rad] angular scale of lenslet on sky
        d_spot = self.f_lenslet / self.F_tel # [um] geometric pupil size in lenslet focal plane
        d_lens_PSF = self.lam*F_lenslet # [um] lenslet diffraction spot size in lenslet focal plane
        d_spot_det = d_spot*self.mag_backend # [um] geometric pupil size on detector
        d_lens_PSF_det = d_lens_PSF*self.mag_backend # [um] lenslet diffraction spot size on detector
        d_spot_pix = d_spot_det/self.d_det_pix # [pix] geometric pupil size in detector samples
        self.d_lens_PSF_pix = d_lens_PSF_det/self.d_det_pix # [pix] lenslet diffraction spot size in detector samples


        # define grids
        #

        # psf grid
        n_psf_samp = 96 # number of samples across PSF patch
        self.d_psf_samp = 1./8 # [pix] scale of PSF samples in detector pixel units
        self.d_psf_samp_f = self.d_psf_samp*self.d_det_pix/self.mag_backend # [um] sample size in lenslet focal plane
        yy_psf, xx_psf = n.mgrid[0:n_psf_samp,0:n_psf_samp]
        yy_psf = (yy_psf-n_psf_samp//2)*self.d_psf_samp
        xx_psf = (xx_psf-n_psf_samp//2)*self.d_psf_samp


        # lenslet grid
        d_lens_samp = 1./n_psf_samp/self.d_psf_samp # [1/pix]  sampling in lenslet plane
        d_lens_samp_x = 1./n_psf_samp/self.d_psf_samp_f * self.lam*self.f_lenslet # [um] sample size in lenslet plane
        D_lens_samp = self.D_lenslet/d_lens_samp_x*d_lens_samp # [1/pix]  size of lenslet in sampled plane
        self.n_lens_samp = max(int(2**n.ceil(n.log2(D_lens_samp/d_lens_samp))),n_psf_samp)
        yy_lens, xx_lens = n.mgrid[0:self.n_lens_samp,0:self.n_lens_samp]
        yy_lens = (yy_lens-self.n_lens_samp//2)*d_lens_samp # [1/pix]
        xx_lens = (xx_lens-self.n_lens_samp//2)*d_lens_samp # [1/pix]
        rr_lens = n.sqrt(xx_lens**2+yy_lens**2) # [1/pix]
        th_lens = n.arctan2(yy_lens, xx_lens)


        # thumbnail grid
        self.w_thumb = n.index_exp[self.n_lens_samp//2-n_psf_samp//2:self.n_lens_samp//2+n_psf_samp//2,
                              self.n_lens_samp//2-n_psf_samp//2:self.n_lens_samp//2+n_psf_samp//2]
        self.xx_psf_t = xx_psf[self.w_thumb]
        self.yy_psf_t = yy_psf[self.w_thumb]


        # rotated lenslet grid
        sa, ca = n.sin(self.ang_lens), n.cos(self.ang_lens)
        xp_lens = xx_lens*ca-yy_lens*sa
        yp_lens = xx_lens*sa+yy_lens*ca

        # lenslet pupil function (square)
        lens_pup = n.zeros((self.n_lens_samp,self.n_lens_samp), dtype=n.float)
        self.pup_w = n.nonzero((yp_lens >= -D_lens_samp/2.) & (yp_lens <= D_lens_samp/2.) &
                               (xp_lens >= -D_lens_samp/2.) & (xp_lens <= D_lens_samp/2.))
        lens_pup[self.pup_w] = 1.


        # coordinates for lens-plane aberrations
        rr = rr_lens / (n.sqrt(2)*D_lens_samp/2.) # radial coordinates so that r=1 at D_lens_samp
        th = th_lens
        rr_t = rr[self.pup_w] # thumbnail
        th_t = th[self.pup_w]

        # check grid sampling
        # FIXME



        # pixel response OTF
        self.otf_pix = n.sinc(xx_lens)*n.sinc(yy_lens)
        self.otf_pix = fftshift(self.otf_pix)[:,:self.n_lens_samp//2+1]

        # geometric pupil OTF
        #otf_pup = 


        # pre-compute zernikes
        self.n_z = zernike.get_n_zern(zern_order)
        self.zernikes = n.array([zernike.get_zernike(j+4,rr_t,th_t) for j in range(self.n_z-3)])

    # ideas for speedups:
    # - use fftshift on coordinate arrays prior to computation
    # - use direct FT computation in heavily zero-padded cases (lenslet to focal field, also getting epsf thumbnail)
    def get_psf(self, zern_coeff, show=False):
        self.opd_t = n.einsum('i,ij',zern_coeff,self.zernikes)
        pup_field = n.zeros((self.n_lens_samp,self.n_lens_samp), dtype=n.complex)
        pup_field[self.pup_w] = n.exp(1j*self.opd_t*2.*n.pi/self.lam) # assumes amplitude = 1

        # compute PSF (monochromatic)
        foc_field = fftshift(fft2(fftshift(pup_field)))
        psf_mono = foc_field.real**2+foc_field.imag**2
        psf_mono /= psf_mono.sum()*self.d_psf_samp**2

        # compute OTF (monochromatic)
        #otf_mono = fftshift(fft2(fftshift(psf_mono)))
        otf_mono = rfft2(psf_mono)

        # compute polychromatic OTF
        otf = otf_mono # FIXME

        # apply geometric pupil OTF
        # FIXME

        # apply pixel response OTF
        otf *= self.otf_pix

        # compute effective PSF
        #epsf = fftshift(ifft2(fftshift(otf))).real
        epsf = irfft2(otf)

        # extract thumbnails
        epsf = epsf[self.w_thumb]


        if show:
            # set up figure
            from image import get_extent
            fig = pylab.figure(0, figsize=(8,4))
            fig.clear()
            ax1 = fig.add_subplot(121)
            ax2 = fig.add_subplot(122)

            # show PSF
            psf = psf_mono # TEMP
            psf = psf[self.w_thumb]
            ax1.imshow(psf,
                       interpolation='nearest',
                       extent=get_extent(psf.shape,scale=self.d_psf_samp,cen=n.array(psf.shape)/2),
                       #cmap=mpl.cm.viridis,
                       cmap=mpl.cm.gist_stern,
                       )

            # show ePSF
            ax2.imshow(epsf,
                       interpolation='nearest',
                       extent=get_extent(psf.shape,scale=self.d_psf_samp,cen=n.array(psf.shape)/2),
                       #cmap=mpl.cm.viridis,
                       cmap=mpl.cm.gist_stern,
                       )



            from matplotlib.patches import Rectangle, Circle
            for ax in (ax1, ax2):
                # draw box for detector pixel
                ax.add_patch(Rectangle((-0.5,-0.5),1.,1.,
                                       fill=None, alpha=1.,
                                       edgecolor='g',
                                       ))

                # draw circle for expected PSF size
                ax.add_patch(Circle((0.,0.), self.d_lens_PSF_pix/2.,
                                    fill=None, alpha=1.,
                                    edgecolor='g',
                                    ))

            ax1.set_title('PSF')
            ax2.set_title('ePSF')

            pylab.draw()
            pylab.show()

        return epsf


class GPIPSFModel(PSFModel):

    D_tel = 8e6 # [um] telescope diameter   FIXME  should be projected Lyot
    D_tel_obs = 1e6 # [um] central obscuration diameter  FIXME  should be projected Lyot
    F_tel = 200. # focal ratio coming into lenslet array
    D_lenslet = 110. # [um] size of lenslet (roughly pitch)
    f_lenslet = 578. # [um] effective focal length of lenslet
    mag_backend = 5.89/3.52 # magnification of spectrometer backend to detector
    d_det_pix = 18. # [um] physical size of detector pixel
    ang_lens = 23.5*n.pi/180. # [rad]  angle of lenslet array relative to detector (CW)


if __name__=='__main__':
    pm = GPIPSFModel()
    zern_coeff = n.zeros(pm.n_z-3, dtype=n.float) # [um]
    zern_coeff[0] = 0.2 # [um]
    zern_coeff[1] = -0.2 # [um]
    zern_coeff[2] = 0. # [um]
    pm.get_psf(zern_coeff, show=True)
