import os
import numpy as n
import matplotlib as mpl
import pylab
import pickle
from astropy.io import fits

import logging
_log = logging.getLogger('gpi')

#import ipdb

import simulate
import psfmodel

data_dir = 'data/gpi/'

def multi_test(height, width):
        "testing multiprocessing"
        """"
        data = n.arange(n_trial)
        def worker(input, output):
            for i in iter(input.get, 'STOP'):
                j = data[i]**2
                output.put((i, j))
        def feeder(input):
            "place locations on input queue"
            for i in range(n_trial):
                input.put(i, True)
            _log.debug('feeder finished')"""
        
        data = n.arange(width*height)##.reshape((height, width))
        
        def worker(input, output):
            for i in iter(input.get, 'STOP'):
                j = data[i]**2-1
                output.put((i, j))
        def feeder(input):
            "place locations on input queue"
            for i in range(height*width):
                input.put(i, True)
            _log.debug('feeder finished')
        
        import multiprocessing as mp
        n_process = mp.cpu_count()
        n_max = n_process*8
        _log.debug("{0} processes".format(n_process))
        inqueue = mp.Queue(n_max)
        outqueue = mp.Queue()
        
        # start worker processes
        for i in range(n_process):
            mp.Process(target=worker, args=(inqueue, outqueue)).start()
        
        # start feeder
        feedp = mp.Process(target=feeder, args=(inqueue,))
        feedp.start()
        
        # collect output
        opt_parms = n.empty_like((input), dtype=n.float)
        outvals = n.empty(height*width, dtype=n.int)
        collect_values = []
        collect_output = []
        for k in range(height*width):
            i, j = outqueue.get()
            outvals[i] = j
            collect_values = n.append(collect_values, j)
        collect_output = n.append(collect_output, outvals)
	    
        # kill worker processes
        _log.debug('received all data; killing workers ...')
        for i in range(n_process):
            inqueue.put('STOP')
            
        # block until feeder finished
        _log.debug('waiting for feeder to finish ...')
        feedp.join(1.)
        _log.debug('... done.')
        
        print(outvals)

        #fh = open("test2.txt","w")
        #fh.write(str(outvals))
        #fh.close()
        
        to_write = str(outvals)
        pickle.dump(to_write, open("testpickle.txt","wb"))
        
        hl = fits.HDUList([fits.PrimaryHDU(outvals)
                               ])
        hl.writeto('testpython.fits', overwrite=True)
        return outvals
        
        
if __name__=='__main__':
    logging.basicConfig(level=logging.DEBUG,
    #logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(name)-12s: %(levelname)-8s %(message)s')
    multi_test(4,6)