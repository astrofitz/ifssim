pro wrapping
  ;; start Python logging
;  logging = Python.Import('logging')
;  format = '%(asctime)s %(name)-12s: %(levelname)-8s %(message)s'
;  void = logging.basicConfig(format = format)
;  Python.Run, 'logging.getLogger().setLevel(logging.DEBUG)'

  ;; execute Python code for model generation
  test = Python.Import('test')
  output = test.multi_test(4,6)
  out_filename='testidl.fits'
  writefits, out_filename, output
end